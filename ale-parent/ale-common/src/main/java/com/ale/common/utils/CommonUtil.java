package com.ale.common.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class CommonUtil {

    public static String getErrorMessage(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            StringBuilder sb = new StringBuilder();
            for (ObjectError error : bindingResult.getAllErrors()) {
                sb.append(error.getDefaultMessage()).append(";");
            }
            return sb.toString();
        }
        return null;
    }

}
