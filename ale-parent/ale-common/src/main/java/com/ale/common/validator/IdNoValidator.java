package com.ale.common.validator;

import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 身份证号验证
 *
 * @Author: zhanxuewei
 * @Date: 2021/11/17 11:24
 */
@Slf4j
public class IdNoValidator implements ConstraintValidator<IdNo, String> {

    @Override
    public void initialize(IdNo idNo) {
        log.info("IdNoValidator : {}", idNo);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            //当状态为空时使用默认值
            return true;
        }
        if (StrUtil.isNotBlank(value)) {
            return IdcardUtil.isValidCard(value);
        } else {
            return false;
        }
    }
}
