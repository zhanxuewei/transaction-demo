package com.ale.common.exception;


import com.ale.common.enums.IErrorCode;
import lombok.Getter;

import java.io.Serializable;

/**
 * 业务异常处理
 *
 * @author zhanxuewei
 */
@Getter
public class BizException extends RuntimeException implements Serializable {

    private Long code;

    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(String message, Long code) {
        super(message);
        this.code = code;
    }

    public BizException(IErrorCode iErrorCode) {
        super(iErrorCode.getMessage());
        this.code = iErrorCode.getCode();
    }

    public BizException(String message, Throwable cause, Long code) {
        super(message, cause);
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public static BizException createByCodeEnum(IErrorCode iErrorCode) {
        return new BizException(iErrorCode.getMessage(), iErrorCode.getCode());
    }

    public static BizException createByCodeEnumAndAttach(IErrorCode iErrorCode, String attach) {
        return new BizException(iErrorCode.getMessage() + attach, iErrorCode.getCode());
    }

}
