package com.ale.common.api;

import com.ale.common.enums.IErrorCode;
import com.ale.common.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 通用响应对象
 *
 * @author admin
 */
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = 899231L;

    /**
     * 状态码
     */
    @ApiModelProperty(value = "状态码", position = 0)
    @JsonProperty(index = 0)
    private Long code;

    /**
     * 消息
     */
    @ApiModelProperty(value = "消息", position = 1)
    @JsonProperty(index = 1)
    private String message;

    /**
     * 响应数据
     */
    @ApiModelProperty(value = "响应数据", position = 2)
    @JsonProperty(index = 2)
    private T data;

    protected ApiResult() {
    }

    /**
     * 构造响应对象
     *
     * @param code    状态码
     * @param message 消息
     * @param data    数据
     */
    public ApiResult(Long code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 响应成功结果
     *
     * @return
     */
    public static <T> ApiResult<T> success() {
        return new ApiResult<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), null);
    }

    public static <T> ApiResult<T> success(Long code, String message) {
        return new ApiResult<>(code, message, null);
    }

    public static <T> ApiResult<T> success(String message) {
        return new ApiResult<>(ResultCode.SUCCESS.getCode(), message, null);
    }

    public static <T> ApiResult<T> success(T data) {
        return new ApiResult<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    public static <T> ApiResult<T> success(Long code, String message, T data) {
        return new ApiResult<>(code, message, data);
    }

    public static <T> ApiResult<T> success(String message, T data) {
        return new ApiResult<>(ResultCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 响应成功结果
     *
     * @param errorCode
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> success(IErrorCode errorCode, T data) {
        return new ApiResult<>(errorCode.getCode(), errorCode.getMessage(), data);
    }

    public static <T> ApiResult<T> success(IErrorCode errorCode) {
        return success(errorCode, null);
    }


    public static <T> ApiResult<T> failed() {
        return new ApiResult<>(ResultCode.FAILED.getCode(), ResultCode.FAILED.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param code
     * @param message
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> failed(Long code, String message) {
        return new ApiResult<>(code, message, null);
    }

    /**
     * 失败返回结果
     *
     * @param message
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> failed(String message) {
        return new ApiResult<>(ResultCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> failed(IErrorCode errorCode) {
        return new ApiResult<>(errorCode.getCode(), errorCode.getMessage(), null);
    }

    public static <T> ApiResult<T> failed(IErrorCode errorCode, T data) {
        return new ApiResult<>(errorCode.getCode(), errorCode.getMessage(), data);
    }

    /**
     * 失败返回结果
     *
     * @param message
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ApiResult<T> failed(String message, T data) {
        return new ApiResult<>(ResultCode.FAILED.getCode(), message, data);
    }


    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CommonResult{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
