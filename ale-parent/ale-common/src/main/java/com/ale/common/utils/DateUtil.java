package com.ale.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class DateUtil {

    //计算两个时间相差的秒数
    public static long getDifSeconds(Date startTime, Date endTime) {
        try {
            if (Objects.nonNull(startTime) && Objects.nonNull(endTime)) {
                long sTime = startTime.getTime();
                long eTime = endTime.getTime();
                return (eTime - sTime) / 1000;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static String format(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }
}
