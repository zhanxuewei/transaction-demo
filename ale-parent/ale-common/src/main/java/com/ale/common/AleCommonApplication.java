package com.ale.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AleCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(AleCommonApplication.class, args);
    }

}
