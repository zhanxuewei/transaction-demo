package com.ale.common.entity.user;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UserListQuery 显示对象", description="用户信息表")
public class UserListQuery {

    @ApiModelProperty(value = "当前页")
    private Long current;

    @ApiModelProperty(value = "每页显示条数")
    private Long size;

    @ApiModelProperty(value = "用户账户")
    private String account;

    @ApiModelProperty(value = "用户姓名")
    private String name;

    @ApiModelProperty(value = "用户性别：0未知 1男 2女")
    private Integer sex;

    @ApiModelProperty(value = "用户手机号")
    private String phone;


}
