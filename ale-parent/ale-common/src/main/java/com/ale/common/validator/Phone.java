package com.ale.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 手机号验证
 *
 * @author zhanxuewei
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = PhoneValidator.class)
public @interface Phone {
    
    String value() default "";

    String message() default "错误的手机号码";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
