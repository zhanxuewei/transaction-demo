package com.ale.common.mapper.order;

import com.ale.common.entity.order.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单主表 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderMapper extends BaseMapper<OrderEntity> {

}
