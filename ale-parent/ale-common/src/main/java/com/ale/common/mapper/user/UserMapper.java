package com.ale.common.mapper.user;

import com.ale.common.entity.user.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
