package com.ale.common.message;

import com.ale.common.entity.product.ProductVO;
import com.ale.common.entity.user.UserVO;
import com.ale.common.feign.dto.product.ProductItemDTO;
import com.ale.common.feign.dto.product.ProductItemInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 扣减库存消息内容
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MqReduceStockDTO 显示对象", description = "扣减库存消息内容")
public class MqReduceStockDTO implements Serializable {

    @ApiModelProperty(value = "事务消息id")
    private String transactionId;

    @ApiModelProperty(value = "商品信息")
    List<ProductItemDTO> productItemDTOList;

    @ApiModelProperty(value = "用户信息")
    private UserVO userVO;

    private Integer sex;

}
