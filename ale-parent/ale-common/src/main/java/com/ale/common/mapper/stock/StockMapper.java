package com.ale.common.mapper.stock;

import com.ale.common.entity.stock.StockEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface StockMapper extends BaseMapper<StockEntity> {

}
