package com.ale.common.constant;

public interface Constants {




    public static final String UTF8 = "UTF-8";
    public static final String GBK = "GBK";
    public static final String AES_KEY = "AES_KEY";
    public static final String DATETIME_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";



    /**
     * 用户状态 : 0 未禁用，1 已禁用
     */
    public static final Boolean USER_DISABLED = false;
    public static final Boolean USER_AVAILABLE = true;

    /**
     * 数据状态 1正常 0删除
     */
    public static final String STATUS_STANDARD = "1";
    public static final String STATUS_DELETE = "0";

}



