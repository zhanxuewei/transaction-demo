package com.ale.common.validator;

import cn.hutool.core.util.PhoneUtil;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 手机号校验器
 *
 * @Author: zhanxuewei
 * @Date: 2021/11/17 11:24
 */
public class PhoneValidator implements ConstraintValidator<Phone, String> {

    @Override
    public void initialize(Phone phone) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            //当状态为空时使用默认值
            return true;
        }
        if (StringUtils.isNotBlank(value)) {
            return PhoneUtil.isMobile(value);
        } else {
            return false;
        }
    }
}
