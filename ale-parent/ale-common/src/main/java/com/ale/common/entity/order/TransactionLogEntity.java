package com.ale.common.entity.order;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 事务日志
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_transaction_log")
public class TransactionLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 事务日志id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 事务id
     */
    private String transactionId;

    /**
     * 日志内容
     */
    private String log;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 乐观锁
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 删除状态 : 1正常，0删除
     */
    @TableLogic
    private Integer status;


}
