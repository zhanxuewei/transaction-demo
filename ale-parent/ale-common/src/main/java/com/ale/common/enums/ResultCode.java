package com.ale.common.enums;


/**
 * 枚举了一些常用API操作码
 *
 * @author zhanxuewei
 */
public enum ResultCode implements IErrorCode {

    // 操作成功
    SUCCESS(200, "操作成功"),

    // 登陆成功
    LOGIN_SUCCESS(200, "登陆成功"),

    // 退出成功
    LOGOUT_SUCCESS(200, "退出成功"),

    // 操作失败
    FAILED(500, "操作失败"),

    // 用户名不存在
    LOGIN_USER_NAME_NOT_FOUND(10002, "用户名不存在"),

    // 账户被锁定
    LOCKED_EXCEPTION(10003, "账户被锁定"),

    // 密码过期
    CREDENTIALS_EXPIRED_EXCEPTION(10004, "密码过期"),

    // 账户过期
    ACCOUNT_EXPIRED_EXCEPTION(10005, "账户过期"),

    // 账户被禁用
    DISABLED_EXCEPTION(10006, "账户被禁用"),

    // 用户名或密码错误
    USERNAME_OR_PASSWORD_ERROR(10007, "用户名或密码错误"),

    // 无效TOKEN
    INVALID_TOKEN(10008, "无效TOKEN"),

    // token不存在
    NO_TOKEN(10009, "token不存在"),

    // 客户端认证失败
    CLIENT_AUTHENTICATION_FAILED(10010, "客户端认证失败"),

    // 不支持的认证模式
    UNSUPPORTED_GRANT_TYPE(10011, "不支持的认证模式"),

    // 参数格式校验失败
    INVALID_ERROR(10012, "参数格式校验失败"),

    // 上传文件大小超出限制
    FILE_SIZE_TOO_BIG_ERROR(10013, "上传文件大小超出限制"),

    // 参数类型错误
    PARAMETER_TYPE_ERROR(10014, "参数类型错误"),

    // 服务异常
    OTHER_ERROR(10015, "服务异常"),
    CATEGORY_NOT_FOUND(10016, "资源分类不存在"),
    CATEGORY_NAME_REPEAT(10017, "分类名称重复"),
    BLACK_FORBIDDEN(10018, "黑名单禁止访问"),

    // 商品信息
    PRODUCT_NOT_FOUND(20019,"商品不存在"),

    // 订单信息
    ORDER_NOT_FOUND(30001,"订单不存在"),
    ORDER_PRODUCT_NOT_FOUND(30002,"订单商品信息不存在"),
    ORDER_USER_NOT_FOUND(30003,"订单用户信息不存在"),

    ;

    private final long code;
    private final String message;

    ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
