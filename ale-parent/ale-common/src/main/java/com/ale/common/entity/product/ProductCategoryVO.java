package com.ale.common.entity.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 商品类目表
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductCategoryVO 显示对象", description = "商品类目表")
public class ProductCategoryVO {

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "商品分类id")
    private Long id;

    @ApiModelProperty(value = "商品分类名称")
    private String categoryName;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "上级id")
    private Long parentId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}
