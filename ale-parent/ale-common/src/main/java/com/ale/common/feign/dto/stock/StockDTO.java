package com.ale.common.feign.dto.stock;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 库存增加、扣减实体
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockDTO 显示对象", description = "库存增加、扣减实体")
public class StockDTO implements Serializable {

    @NotNull(message = "商品id不能为空")
    @ApiModelProperty(value = "商品id")
    private Long productId;

    @NotNull(message = "数量不能为空")
    @ApiModelProperty(value = "数量")
    private Integer number;

}
