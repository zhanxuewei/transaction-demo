package com.ale.common.mapper.order;

import com.ale.common.entity.order.OrderUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单用户信息表 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderUserMapper extends BaseMapper<OrderUserEntity> {

}
