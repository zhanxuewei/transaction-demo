package com.ale.common.feign.dto.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 购买商品明细
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductItemInput 显示对象", description = "购买商品明细")
public class ProductItemInput implements Serializable {

    @NotNull(message = "商品id不能为空")
    @ApiModelProperty(value = "商品id")
    private Long productId;

    @NotNull(message = "数量不能为空")
    @ApiModelProperty(value = "数量")
    private Integer number;

}
