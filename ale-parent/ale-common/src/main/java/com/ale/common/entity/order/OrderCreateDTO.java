package com.ale.common.entity.order;

import com.ale.common.feign.dto.product.ProductItemInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 创建订单参数
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OrderCreateDTO 显示对象", description = "创建订单参数")
public class OrderCreateDTO {

    @NotNull(message = "用户id不能为空")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    @NotEmpty(message = "商品明细不能为空")
    @ApiModelProperty(value = "商品明细")
    private List<ProductItemInput> productItemInputs;

    @ApiModelProperty(value = "测试字段")
    private Integer sex;


}
