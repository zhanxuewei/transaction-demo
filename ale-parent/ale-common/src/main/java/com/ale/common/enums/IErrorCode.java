package com.ale.common.enums;

/**
 * 错误模型接口
 *
 */
public interface IErrorCode {

    /**
     * 状态码
     *
     * @return long
     */
    long getCode();

    /**
     * 错误消息
     *
     * @return string
     */
    String getMessage();

}
