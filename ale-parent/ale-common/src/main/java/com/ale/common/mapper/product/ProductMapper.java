package com.ale.common.mapper.product;

import com.ale.common.entity.product.ProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface ProductMapper extends BaseMapper<ProductEntity> {

}
