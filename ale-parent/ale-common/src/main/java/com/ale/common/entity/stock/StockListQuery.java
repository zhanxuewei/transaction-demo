package com.ale.common.entity.stock;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StockListQuery 显示对象", description = "")
public class StockListQuery {

    @NotNull(message = "页码不能为空")
    @ApiModelProperty(value = "页码")
    private Long current;

    @NotNull(message = "每页显示条数不能为空")
    @ApiModelProperty(value = "每页显示条数")
    private Long size;

    @ApiModelProperty(value = "商品名称")
    private String productName;

    @ApiModelProperty(value = "商品分类名称")
    private String productCategoryName;


}
