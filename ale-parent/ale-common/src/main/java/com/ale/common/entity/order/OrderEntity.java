package com.ale.common.entity.order;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单主表
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_order")
public class OrderEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单主表id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 订单编号
     */
    private String orderNumber;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 订单支付状态：0待支付 1已支付
     */
    private Integer orderPayStatus;

    /**
     * 支付方式：1微信 2支付宝 3其他
     */
    private Integer orderPayType;

    /**
     * 支付时间
     */
    private Date orderPayTime;

    /**
     * 订单状态：0取消 1正常
     */
    private Integer orderStatus;

    /**
     * 乐观锁
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 删除状态 : 1正常，0删除
     */
    @TableLogic
    private Integer status;


}
