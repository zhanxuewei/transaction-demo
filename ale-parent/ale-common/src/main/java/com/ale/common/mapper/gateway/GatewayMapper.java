package com.ale.common.mapper.gateway;

import com.ale.common.entity.gateway.GatewayEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface GatewayMapper extends BaseMapper<GatewayEntity> {

}
