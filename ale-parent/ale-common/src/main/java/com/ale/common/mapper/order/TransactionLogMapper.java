package com.ale.common.mapper.order;

import com.ale.common.entity.order.TransactionLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 事务日志 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
public interface TransactionLogMapper extends BaseMapper<TransactionLogEntity> {

}
