package com.ale.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 身份证号验证
 *
 * @author zhanxuewei
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = IdNoValidator.class)
public @interface IdNo {
    
    String value() default "";

    String message() default "错误的身份号码";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
