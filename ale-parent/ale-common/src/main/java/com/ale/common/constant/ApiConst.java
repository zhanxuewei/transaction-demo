package com.ale.common.constant;

/**
 * 接口常量
 *
 * @Author: zhanxuewei
 * @Date: 2021/10/28 11:38
 */
public class ApiConst {

    /**
     * api 前缀
     */
    public static final String API_PREFIX = "/api/v1";

}
