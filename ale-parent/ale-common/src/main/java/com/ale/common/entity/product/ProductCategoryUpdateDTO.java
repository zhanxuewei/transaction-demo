package com.ale.common.entity.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 商品类目表
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductCategoryUpdateDTO 显示对象", description = "商品类目表")
public class ProductCategoryUpdateDTO {

    @NotNull(message = "商品分类id不能为空")
    @ApiModelProperty(value = "商品分类id")
    private Long id;

    @NotBlank(message = "商品分类名称不能为空")
    @ApiModelProperty(value = "商品分类名称")
    private String categoryName;

//    @NotNull(message = "上级id不能为空")
//    @ApiModelProperty(value = "上级id")
//    private Long parentId;


}
