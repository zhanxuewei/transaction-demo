package com.ale.common.entity.order;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 事务日志
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TransactionLogQuery 显示对象", description="事务日志")
public class TransactionLogQuery {

    @ApiModelProperty(value = "事务日志id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "事务id")
    private String transactionId;

    @ApiModelProperty(value = "日志内容")
    private String log;

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Long version;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "删除状态 : 1正常，0删除")
    @TableLogic
    private Integer status;


}
