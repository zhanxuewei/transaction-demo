package com.ale.common.mapper.order;

import com.ale.common.entity.order.OrderProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单商品信息表 Mapper 接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderProductMapper extends BaseMapper<OrderProductEntity> {

}
