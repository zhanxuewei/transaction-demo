package com.ale.common.entity.order;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单商品信息表
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_order_product")
public class OrderProductEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单商品信息id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品类目id
     */
    private Long productCategoryId;

    /**
     * 商品类目名称
     */
    private String productCategoryName;

    /**
     * 商品数量
     */
    private Integer productQuantity;

    /**
     * 商品价格
     */
    private BigDecimal productPrice;

    /**
     * 乐观锁
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 删除状态 : 1正常，0删除
     */
    @TableLogic
    private Integer status;


}
