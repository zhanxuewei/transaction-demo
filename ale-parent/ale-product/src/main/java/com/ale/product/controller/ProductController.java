package com.ale.product.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.product.ProductListQuery;
import com.ale.common.entity.product.ProductSaveDTO;
import com.ale.common.entity.product.ProductUpdateDTO;
import com.ale.common.entity.product.ProductVO;
import com.ale.common.enums.ResultCode;
import com.ale.product.service.ProductService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 商品信息接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "商品信息接口", description = "ProductController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody ProductSaveDTO saveDTO) {
        productService.save(saveDTO);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<ProductVO>> list(ProductListQuery productListQuery) {
        IPage<ProductVO> page = productService.list(productListQuery);
        return ApiResult.success(PageResult.restPage(page, page.getRecords()));
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody @Validated ProductUpdateDTO updateDTO) {
        productService.update(updateDTO);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<Long> ids) {
        productService.deletes(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail")
    public ApiResult<ProductVO> detail(@RequestParam("id") Long id) {
        ProductVO vo = productService.detail(id);
        return ApiResult.success(vo);
    }

    @ApiOperation("根据id集合查询")
    @PostMapping("/queryByIds")
    public ApiResult<List<ProductVO>> queryByIds(@RequestBody List<Long> ids) {
        List<ProductVO> result = productService.queryByIds(ids);
        return ApiResult.success(result);
    }

}

