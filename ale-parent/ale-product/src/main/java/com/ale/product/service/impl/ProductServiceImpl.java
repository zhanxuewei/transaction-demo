package com.ale.product.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ale.common.entity.product.*;
import com.ale.common.mapper.product.ProductMapper;
import com.ale.product.service.ProductService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品信息表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, ProductEntity> implements ProductService {


    @Override
    public boolean save(ProductSaveDTO saveDTO) {
        ProductEntity productEntity = new ProductEntity();
        BeanUtils.copyProperties(saveDTO, productEntity);
        return super.save(productEntity);
    }

    @Override
    public boolean deletes(List<Long> ids) {
        return super.removeByIds(ids);
    }

    @Override
    public boolean update(ProductUpdateDTO updateDTO) {
        ProductEntity productEntity = new ProductEntity();
        BeanUtils.copyProperties(updateDTO, productEntity);
        return super.updateById(productEntity);
    }

    @Override
    public ProductVO detail(Long id) {
        ProductEntity productEntity = super.getById(id);
        if (Objects.isNull(productEntity)) {
            return null;
        }
        ProductVO vo = new ProductVO();
        BeanUtils.copyProperties(productEntity, vo);
        return vo;
    }


    @Override
    public IPage<ProductVO> list(ProductListQuery productListQuery) {
        IPage<ProductEntity> pageInfo = new Page<>(productListQuery.getCurrent(), productListQuery.getSize());
        QueryWrapper<ProductEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<ProductEntity> lambda = queryWrapper.lambda();
        lambda.orderByDesc(ProductEntity::getCreateTime);
        IPage<ProductEntity> page = super.page(pageInfo, queryWrapper);
        return page.convert(item -> {
            ProductVO productVO = new ProductVO();
            BeanUtils.copyProperties(item, productVO);
            return productVO;
        });
    }


    @Override
    public List<ProductVO> queryByIds(List<Long> ids) {
        List<ProductEntity> list = super.listByIds(ids);
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(item -> {
            ProductVO vo = new ProductVO();
            BeanUtils.copyProperties(item, vo);
            return vo;
        }).collect(Collectors.toList());
    }

}
