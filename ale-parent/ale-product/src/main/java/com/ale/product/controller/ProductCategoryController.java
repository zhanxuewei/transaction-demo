package com.ale.product.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.product.*;
import com.ale.common.enums.ResultCode;
import com.ale.product.service.ProductCategoryService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * <p>
 * 商品类目接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "商品类目接口", description = "ProductCategoryController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/product/category")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody @Validated ProductCategorySaveDTO saveDTO) {
        productCategoryService.save(saveDTO.getCategoryName(), saveDTO.getParentId());
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<ProductCategoryVO>> list(ProductCategoryListQuery productCategoryListQuery) {
        IPage<ProductCategoryVO> page = productCategoryService.list(productCategoryListQuery);
        return ApiResult.success(PageResult.restPage(page, page.getRecords()));
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody @Validated ProductCategoryUpdateDTO updateDTO) {
        productCategoryService.updateById(updateDTO.getId(),updateDTO.getCategoryName());
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<Long> ids) {
        productCategoryService.removeByIds(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail")
    public ApiResult<ProductCategoryVO> categoryDetail(@RequestParam("id") @NotNull(message = "商品类目id不能为空") Long id) {
        ProductCategoryVO vo = productCategoryService.detail(id);
        return ApiResult.success(vo);
    }

}

