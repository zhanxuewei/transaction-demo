package com.ale.product.service.impl;

import com.ale.common.entity.product.ProductCategoryEntity;
import com.ale.common.mapper.product.ProductCategoryMapper;
import com.ale.common.entity.product.ProductCategoryListQuery;
import com.ale.common.entity.product.ProductCategoryVO;
import com.ale.product.service.ProductCategoryService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 商品类目表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryMapper, ProductCategoryEntity> implements ProductCategoryService {

    @Override
    public boolean save(String categoryName, Long parentId) {
        ProductCategoryEntity productCategoryEntity = new ProductCategoryEntity();
        productCategoryEntity.setCategoryName(categoryName);
        productCategoryEntity.setParentId(parentId);
        return super.save(productCategoryEntity);
    }

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean removeByIds(List<Long> ids) {
        return super.removeByIds(ids);
    }


    @Override
    public boolean updateById(Long id, String categoryName) {
        ProductCategoryEntity productCategoryEntity = new ProductCategoryEntity();
        productCategoryEntity.setId(id);
        productCategoryEntity.setCategoryName(categoryName);
        return super.updateById(productCategoryEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return ProductCategoryDO
     */
    @Override
    public ProductCategoryVO detail(Long id) {
        ProductCategoryEntity productCategoryEntity = super.getById(id);
        if (Objects.isNull(productCategoryEntity)) {
            return null;
        }
        ProductCategoryVO vo = new ProductCategoryVO();
        BeanUtils.copyProperties(productCategoryEntity, vo);
        return vo;
    }


    @Override
    public IPage<ProductCategoryVO> list(ProductCategoryListQuery productCategoryListQuery) {
        IPage<ProductCategoryEntity> pageInfo = new Page<>(productCategoryListQuery.getCurrent(), productCategoryListQuery.getSize());
        QueryWrapper<ProductCategoryEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<ProductCategoryEntity> lambda = queryWrapper.lambda();
        lambda.orderByDesc(ProductCategoryEntity::getCreateTime);
        IPage<ProductCategoryEntity> page = super.page(pageInfo, queryWrapper);
        return page.convert(item -> {
            ProductCategoryVO productCategoryVO = new ProductCategoryVO();
            BeanUtils.copyProperties(item, productCategoryVO);
            return productCategoryVO;
        });
    }

}
