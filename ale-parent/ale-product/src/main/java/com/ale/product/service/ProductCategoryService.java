package com.ale.product.service;


import com.ale.common.entity.product.ProductCategoryEntity;
import com.ale.common.entity.product.ProductCategoryListQuery;
import com.ale.common.entity.product.ProductCategoryVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品类目表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface ProductCategoryService extends IService<ProductCategoryEntity> {

    /**
     * 添加
     *
     * @param categoryName 商品分类名称
     * @param parentId     父级id
     * @return 操作是否成功
     */
    boolean save(String categoryName, Long parentId);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<Long> ids);

    /**
     * 根据ID修改
     *
     * @param id
     * @param categoryName
     * @return
     */
    boolean updateById(Long id, String categoryName);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return ProductCategoryVO
     */
    ProductCategoryVO detail(Long id);

    /**
     * 条件分页查询
     *
     * @param productCategoryListQuery ProductCategoryListQuery
     * @return IPage<ProductCategoryDO>
     */
    IPage<ProductCategoryVO> list(ProductCategoryListQuery productCategoryListQuery);

}
