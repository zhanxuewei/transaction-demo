package com.ale.product.service;


import com.ale.common.entity.product.ProductEntity;
import com.ale.common.entity.product.ProductListQuery;
import com.ale.common.entity.product.ProductSaveDTO;
import com.ale.common.entity.product.ProductUpdateDTO;
import com.ale.common.entity.product.ProductVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface ProductService extends IService<ProductEntity> {

    /**
     * 添加
     *
     * @param saveDTO ProductQuery
     * @return 是否成功 true|false
     */
    boolean save(ProductSaveDTO saveDTO);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean deletes(List<Long> ids);

    /**
     * 根据ID修改
     *
     * @param updateDTO updateDTO
     * @return 是否成功 true|false
     */
    boolean update(ProductUpdateDTO updateDTO);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return ProductVO
     */
    ProductVO detail(Long id);

    /**
     * 条件分页查询
     *
     * @param productListQuery ProductListQuery
     * @return IPage<ProductDO>
     */
    IPage<ProductVO> list(ProductListQuery productListQuery);


    List<ProductVO> queryByIds(List<Long> ids);
}
