package com.ale.third.controller;

import com.ale.common.constant.ApiConst;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 地址库接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-10-11
 */
@Api(tags = "地址库接口", description = "AddressController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/third/address/")
public class AddressController {


}

