package com.ale.third.service.impl;

import com.ale.third.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-10-11
 */
@Service
@Slf4j
public class AddressServiceImpl implements AddressService {


}
