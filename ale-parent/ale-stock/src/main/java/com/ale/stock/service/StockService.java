package com.ale.stock.service;


import com.ale.common.entity.stock.StockEntity;
import com.ale.common.entity.stock.StockListQuery;
import com.ale.common.entity.stock.StockQuery;
import com.ale.common.entity.stock.StockVO;
import com.ale.common.feign.dto.stock.StockDTO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface StockService extends IService<StockEntity> {

    /**
     * 添加
     *
     * @param stockQuery StockQuery
     * @return 是否成功 true|false
     */
    boolean save(StockQuery stockQuery);

    /**
     * 批量添加
     *
     * @param list List<StockQuery>
     * @return 是否成功 true|false
     */
    boolean saveBatch(List<StockQuery> list);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<String> ids);

    /**
     * 根据ID修改
     *
     * @param stockQuery StockQuery
     * @return 是否成功 true|false
     */
    boolean updateById(StockQuery stockQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return StockDO
     */
    StockEntity getById(String id);

    /**
     * 条件分页查询
     *
     * @param stockListQuery StockListQuery
     * @return IPage<StockDO>
     */
    IPage<StockVO> list(StockListQuery stockListQuery);

    void reduce(List<StockDTO> stockDTOList);

    void increase(List<StockDTO> stockDTOList);

    void stockChange(List<StockDTO> stockDTOList, Boolean flag);
}
