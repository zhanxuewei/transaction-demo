package com.ale.stock.listener;

import com.ale.common.feign.dto.product.ProductItemDTO;
import com.ale.common.feign.dto.stock.StockDTO;
import com.ale.common.message.MqReduceStockDTO;
import com.ale.stock.service.StockService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RocketMQMessageListener(topic = "reduce-stock", consumerGroup = "consumer-group")
public class ReduceStockListener implements RocketMQListener<String> {

    @Autowired
    private StockService stockService;

    @Override
    public void onMessage(String payload) {
        log.info("接收消息：{}", payload);
        MqReduceStockDTO mqReduceStockDTO = JSON.parseObject(payload, MqReduceStockDTO.class);
        if (mqReduceStockDTO.getSex() % 2 == 0) {
            int i = 1 / 0;
            //这个地方抛出异常，消息状态会是UNKNOWN状态
        }
        List<ProductItemDTO> productItemDTOList = mqReduceStockDTO.getProductItemDTOList();
        List<StockDTO> collect = productItemDTOList.stream().map(item -> {
            StockDTO stockDTO = new StockDTO();
            stockDTO.setProductId(item.getId());
            stockDTO.setNumber(item.getNumber());
            return stockDTO;
        }).collect(Collectors.toList());
        stockService.reduce(collect);
        log.info("扣减库存成功");
    }
}
