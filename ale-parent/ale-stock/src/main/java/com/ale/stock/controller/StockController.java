package com.ale.stock.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.stock.StockEntity;
import com.ale.common.entity.stock.StockListQuery;
import com.ale.common.entity.stock.StockQuery;
import com.ale.common.entity.stock.StockVO;
import com.ale.common.enums.ResultCode;
import com.ale.common.feign.dto.stock.StockDTO;
import com.ale.stock.service.StockService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 库存接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "库存接口", description = "StockController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/stock")
public class StockController {

    @Autowired
    private StockService stockService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody StockQuery stockQuery) {
        stockService.save(stockQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<StockVO>> list(@Validated StockListQuery stockListQuery) {
        IPage<StockVO> page = stockService.list(stockListQuery);
        return ApiResult.success(PageResult.restPage(page, page.getRecords()));
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody StockQuery stockQuery) {
        stockService.updateById(stockQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<Long> ids) {
        stockService.removeByIds(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail")
    public ApiResult<StockVO> detail(@RequestParam("id") Long id) {
        StockEntity stockEntity = stockService.getById(id);
        StockVO stockVO = new StockVO();
        BeanUtils.copyProperties(stockEntity, stockVO);
        return ApiResult.success(stockVO);
    }

    @ApiOperation("扣减库存")
    @PostMapping("/reduce")
    public ApiResult<ResultCode> reduce(@RequestBody List<StockDTO> stockDTOList) {
        stockService.reduce(stockDTOList);
        return ApiResult.success();
    }

    @ApiOperation("增加库存")
    @PostMapping("/increase")
    public ApiResult<ResultCode> increase(@RequestBody List<StockDTO> stockDTOList) {
        stockService.increase(stockDTOList);
        return ApiResult.success();
    }

}

