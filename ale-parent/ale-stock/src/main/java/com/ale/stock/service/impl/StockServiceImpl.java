package com.ale.stock.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ale.common.entity.stock.StockEntity;
import com.ale.common.entity.stock.StockListQuery;
import com.ale.common.entity.stock.StockQuery;
import com.ale.common.entity.stock.StockVO;
import com.ale.common.enums.ResultCode;
import com.ale.common.exception.BizException;
import com.ale.common.feign.dto.stock.StockDTO;
import com.ale.common.mapper.stock.StockMapper;
import com.ale.stock.service.StockService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class StockServiceImpl extends ServiceImpl<StockMapper, StockEntity> implements StockService {


    /**
     * 添加
     *
     * @param stockQuery StockQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean save(StockQuery stockQuery) {
        StockEntity stockEntity = new StockEntity();
        BeanUtils.copyProperties(stockQuery, stockEntity);
        return super.save(stockEntity);
    }

    /**
     * 批量添加
     *
     * @param list List<StockQuery>
     * @return 是否成功 true|false
     */
    @Override
    public boolean saveBatch(List<StockQuery> list) {
        List<StockEntity> collect = list.stream().map(item -> {
            StockEntity stockEntity = new StockEntity();
            BeanUtils.copyProperties(item, stockEntity);
            return stockEntity;
        }).collect(Collectors.toList());
        return super.saveBatch(collect);
    }

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean removeByIds(List<String> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 根据ID修改
     *
     * @param stockQuery StockQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean updateById(StockQuery stockQuery) {
        StockEntity stockEntity = new StockEntity();
        BeanUtils.copyProperties(stockQuery, stockEntity);
        return super.updateById(stockEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return StockDO
     */
    @Override
    public StockEntity getById(String id) {
        return super.getById(id);
    }

    /**
     * 条件分页查询
     *
     * @param stockListQuery StockListQuery
     * @return IPage<StockDO>
     */
    @Override
    public IPage<StockVO> list(StockListQuery stockListQuery) {
        IPage<StockEntity> pageInfo = new Page<>(stockListQuery.getCurrent(), stockListQuery.getSize());
        QueryWrapper<StockEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<StockEntity> lambda = queryWrapper.lambda();
        lambda.orderByDesc(StockEntity::getCreateTime);
        IPage<StockEntity> page = super.page(pageInfo, queryWrapper);
        return page.convert(item -> {
            StockVO stockVO = new StockVO();
            BeanUtils.copyProperties(item, stockVO);
            return stockVO;
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void reduce(List<StockDTO> stockDTOList) {
        this.stockChange(stockDTOList, false);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void increase(List<StockDTO> stockDTOList) {
        this.stockChange(stockDTOList, true);
    }
    
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void stockChange(List<StockDTO> stockDTOList, Boolean flag) {
        List<StockEntity> list = new ArrayList<>();
        for (StockDTO stockDTO : stockDTOList) {
            LambdaQueryWrapper<StockEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(StockEntity::getProductId, stockDTO.getProductId());
            StockEntity stockEntity = super.getOne(wrapper);
            if (Objects.isNull(stockEntity)) {
                throw new BizException(ResultCode.PRODUCT_NOT_FOUND);
            }
            Integer stock = stockEntity.getStock();
            Integer resultStock = flag ? stock + stockDTO.getNumber() : stock - stockDTO.getNumber();
            stockEntity.setStock(resultStock);
            list.add(stockEntity);
        }
        if (CollUtil.isNotEmpty(list)) {
            super.saveOrUpdateBatch(list);
        }

    }

}
