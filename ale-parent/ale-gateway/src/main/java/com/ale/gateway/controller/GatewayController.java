package com.ale.gateway.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.gateway.GatewayListQuery;
import com.ale.common.entity.gateway.GatewayQuery;
import com.ale.common.entity.gateway.GatewayVO;
import com.ale.common.enums.ResultCode;
import com.ale.gateway.service.GatewayService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 网关接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-06-13
 */
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/gateway")
@Slf4j
public class GatewayController {

    @Autowired
    private GatewayService gatewayService;

    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody GatewayQuery gatewayQuery) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【保存】 参数：gatewayQuery = {}", JSON.toJSONString(gatewayQuery));
        }
        gatewayService.save(gatewayQuery);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【保存】 参数：gatewayQuery = {} 结果：result = {}", JSON.toJSONString(gatewayQuery), ResultCode.SUCCESS.getMessage());
        }
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @GetMapping("/list")
    public ApiResult<PageResult<GatewayVO>> queryList(GatewayListQuery gatewayListQuery) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【分页条件查询】 参数：gatewayListQuery = {}", JSON.toJSONString(gatewayListQuery));
        }
        IPage<GatewayVO> page = gatewayService.queryList(gatewayListQuery);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【分页条件查询】 参数：gatewayListQuery = {} 结果：result = {}", JSON.toJSONString(gatewayListQuery), JSON.toJSONString(page));
        }
        return ApiResult.success(PageResult.restPage(page, page.getRecords()));
    }

    @PostMapping("/saveBatch")
    public ApiResult<ResultCode> saveBatch(@RequestBody List<GatewayQuery> list) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【批量保存】 参数：list = {}", JSON.toJSONString(list));
        }
        gatewayService.saveBatch(list);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【批量保存】 参数：list = {} 结果：result = {}", JSON.toJSONString(list), ResultCode.SUCCESS.getMessage());
        }
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody GatewayQuery gatewayQuery) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【修改】 参数：gatewayQuery = {}", JSON.toJSONString(gatewayQuery));
        }
        gatewayService.updateById(gatewayQuery);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【修改】 参数：gatewayQuery = {} 结果：result = {}", JSON.toJSONString(gatewayQuery), ResultCode.SUCCESS.getMessage());
        }
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<Long> ids) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【批量删除】 参数：ids = {}", JSON.toJSONString(ids));
        }
        gatewayService.removeByIds(ids);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【批量删除】 参数：ids = {} 结果：result = {}", JSON.toJSONString(ids), ResultCode.SUCCESS.getMessage());
        }
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @GetMapping("/detail/{id}")
    public ApiResult<GatewayVO> detail(@PathVariable("id") Long id) {
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【详情】 参数：id = {}", JSON.toJSONString(id));
        }
        GatewayVO gatewayVO = gatewayService.detail(id);
        if (log.isDebugEnabled()) {
            log.info("【网关接口】【详情】】 参数：id = {} 结果：result = {}", JSON.toJSONString(id), JSON.toJSONString(gatewayVO));
        }
        return ApiResult.success(gatewayVO);
    }

}

