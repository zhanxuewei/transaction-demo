package com.ale.gateway.runner;

import com.ale.gateway.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动时加载一次
 */
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

    @Autowired
    private GatewayService gatewayService;

    @Override
    public void run(ApplicationArguments args) {
        System.out.println("通过实现ApplicationRunner接口，在spring boot项目启动后打印参数");
        String[] sourceArgs = args.getSourceArgs();
        for (String arg : sourceArgs) {
            System.out.print(arg + " ");
        }
        System.out.println(">>>动态加载gateway规则...");
        this.gatewayService.initAllRoute();
        System.out.println(">>>加载gateway规则完成！");
    }
}
