package com.ale.gateway.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.enums.ResultCode;
import com.ale.gateway.service.GatewayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 路由接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-05-24
 */
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/gateway/router")
@Slf4j
@Validated
public class RouterController {

    @Autowired
    private GatewayService gatewayService;

    @PostMapping("/init")
    public ApiResult<ResultCode> init() {
        log.info("开始初始化路由...");
        gatewayService.initAllRoute();
        log.info("初始化路由完成.");
        return ApiResult.success(ResultCode.SUCCESS);
    }


}

