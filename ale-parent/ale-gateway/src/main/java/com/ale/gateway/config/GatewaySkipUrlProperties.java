package com.ale.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author admin
 */
@Data
@Component
@ConfigurationProperties(prefix = "skip.url") // 配置文件的前缀
public class GatewaySkipUrlProperties {

    /**
     * 白名单
     */
    private List<String> include;
}