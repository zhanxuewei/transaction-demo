package com.ale.gateway.service;

/**
 * redis操作通用接口
 */
public interface RedisService {
    /**
     * 键值对读取
     *
     * @param key
     * @return
     */
    Object get(String key);

    /**
     * 键值对写入
     *
     * @param key
     * @param value
     * @return
     */
    boolean set(String key, Object value);

    boolean set(String key, Object value, long timeout);

    /**
     * 键值对更新
     *
     * @param key
     * @param value
     * @return
     */
    boolean update(String key, String value);

    /**
     * 键值对删除
     *
     * @param key
     * @return
     */
    boolean delete(String key);

    /**
     * 键值对指定缓存失效时间
     *
     * @param key
     * @return
     */
    boolean expire(String key, long time);

    /**
     * 键值对根据key 获取过期时间
     *
     * @param key
     * @return
     */
    long getExpire(String key);

    /**
     * 键值对判断key是否存在
     *
     * @param key
     * @return
     */
    boolean hasKey(String key);

    /**
     * 键值对可以删除一个值 或多个缓存
     *
     * @param key
     * @return
     */
    boolean delAll(String... key);


    /**
     * 哈希读取
     *
     * @param key
     * @return
     */
    Object hashGet(String hkey, Object key);

    /**
     * 哈希写入
     *
     * @param key
     * @param value
     * @return
     */
    boolean hashSet(String hkey, Object key, Object value);

    /**
     * 哈希删除
     *
     * @param key
     * @return
     */
    boolean hashDelete(String hkey, Object key);

    /**
     * 哈希判断key是否存在
     *
     * @param key
     * @return
     */
    boolean hashHasKey(String hkey, String key);

    /**
     * 哈希可以删除一个值 或多个缓存
     *
     * @param key
     * @return
     */
    boolean hashDelAll(String hkey, String... key);

    /**
     * 获得锁
     */
    boolean getLock(String lockId, long millisecond);

    /**
     * 释放锁
     */
    void releaseLock(String lockId);
}
