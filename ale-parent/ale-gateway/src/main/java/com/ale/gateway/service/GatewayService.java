package com.ale.gateway.service;


import com.ale.common.entity.gateway.GatewayEntity;
import com.ale.common.entity.gateway.GatewayListQuery;
import com.ale.common.entity.gateway.GatewayQuery;
import com.ale.common.entity.gateway.GatewayVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-06-13
 */
public interface GatewayService extends IService<GatewayEntity> {

    /**
     * 添加
     *
     * @param gatewayQuery GatewayQuery
     * @return 是否成功 true|false
     */
    boolean save(GatewayQuery gatewayQuery);

    /**
     * 批量添加
     *
     * @param list List<GatewayQuery>
     * @return 是否成功 true|false
     */
    boolean saveBatch(List<GatewayQuery> list);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<Long> ids);

    /**
     * 根据ID修改
     *
     * @param gatewayQuery GatewayQuery
     * @return 是否成功 true|false
     */
    boolean updateById(GatewayQuery gatewayQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return GatewayDO
     */
    GatewayEntity getById(Long id);

    /**
     * 条件分页查询
     *
     * @param gatewayListQuery GatewayListQuery
     * @return IPage<GatewayDO>
     */
    IPage<GatewayEntity> list(GatewayListQuery gatewayListQuery);

    void initAllRoute();

    IPage<GatewayVO> queryList(GatewayListQuery gatewayListQuery);

    GatewayVO detail(Long id);
}
