package com.ale.gateway.filter;

import com.ale.common.api.ApiResult;
import com.ale.common.entity.user.UserVO;
import com.ale.common.enums.ResultCode;
import com.ale.common.utils.DateUtil;
import com.ale.common.utils.MD5Util;
import com.ale.gateway.config.GatewaySkipUrlProperties;
import com.ale.gateway.service.RedisService;
import com.ale.gateway.utils.EncryptUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 全局filter，认证鉴权过滤器
 */
@Component
@Slf4j
public class GatewayFilterConfig implements GlobalFilter, Ordered {


    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private RedisService redisService;

    @Autowired
    private GatewaySkipUrlProperties gatewaySkipUrlProperties;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("=====================================网关过滤器执行=========================================");
        ServerHttpRequest request = exchange.getRequest();
        String requestUrl = request.getPath().value();
        log.info("时间：【{}】，请求路径：【{}】，", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"), requestUrl);
        // 保存请求日志
        // 黑名单拦截
        // 白名单跳过token校验
        AntPathMatcher pathMatcher = new AntPathMatcher();
        List<String> skipUrls = gatewaySkipUrlProperties.getInclude();
        log.info("白名单列表 skipUrls = {}", JSON.toJSONString(skipUrls));
        for (String url : skipUrls) {
            if (pathMatcher.match(url, requestUrl)) {
                request = exchange.getRequest().mutate().header(HttpHeaders.AUTHORIZATION, "").build();
                exchange = exchange.mutate().request(request).build();
                return chain.filter(exchange);
            }
        }

        // 检查token是否存在
        String token = getToken(exchange);
        log.info("获取header中的token, token = {}", token);
        if (StringUtils.isBlank(token)) {
            return noTokenMono(exchange);
        }

        // 判断是否是有效的token
        OAuth2AccessToken oAuth2AccessToken;
        try {
            oAuth2AccessToken = tokenStore.readAccessToken(token);
            Map<String, Object> additionalInformation = oAuth2AccessToken.getAdditionalInformation();
            log.info("解析token, additionalInformation = {}", JSON.toJSONString(additionalInformation));

            // 取出用户身份信息
            UserVO userVO = new UserVO();
            userVO.setId((Long) additionalInformation.get("id"));
            userVO.setName((String) additionalInformation.get("name"));
            userVO.setPhone((String) additionalInformation.get("phone"));

            // 获取用户权限
            List<String> authorities = (List<String>) additionalInformation.get("authorities");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userVO", userVO);
            jsonObject.put("authorities", authorities);

            // 给header里面添加值
            String base64 = EncryptUtil.encodeUTF8StringBase64(JSON.toJSONString(jsonObject));
            ServerHttpRequest tokenRequest = exchange.getRequest().mutate().header("json-token", base64).build();
            ServerWebExchange build = exchange.mutate().request(tokenRequest).build();

            // 将用户信息存入redis
            redisService.set(MD5Util.md5Encode(token), JSON.toJSONString(userVO), 60 * 60 * 24);
            return chain.filter(build);
        } catch (InvalidTokenException e) {
            log.error("无效的token: {}", token);
            return invalidTokenMono(exchange);
        }

    }


    @Override
    public int getOrder() {
        return 0;
    }


    // 获取token
    private String getToken(ServerWebExchange exchange) {
        String tokenStr = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isBlank(tokenStr)) {
            return null;
        }
        String token = tokenStr.split(" ")[1];
        if (StringUtils.isBlank(token)) {
            return null;
        }
        return token;
    }

    // token不存在
    private Mono<Void> noTokenMono(ServerWebExchange exchange) {
        return buildReturnMono(ApiResult.failed(ResultCode.NO_TOKEN), exchange);
    }

    // 无效的token
    private Mono<Void> invalidTokenMono(ServerWebExchange exchange) {
        return buildReturnMono(ApiResult.failed(ResultCode.INVALID_TOKEN), exchange);
    }

    // 自定义返回结果
    private Mono<Void> buildReturnMono(ApiResult<?> apiResult, ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        byte[] bits = JSONObject.toJSONString(apiResult).getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        response.setStatusCode(HttpStatus.OK);
        //指定编码，否则在浏览器中会中文乱码
        response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }

}