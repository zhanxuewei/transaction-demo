package com.mk.generate.common.constant;

/**
 * Redis Key
 */
public class RedisKeyConst {

    /**
     * 用户操作字典表
     */
    public static final String USER_OPS_MAP = "USER_OPS_MAP";

    /**
     * 舆情信息过程追踪步骤数前缀
     */
    public static final String YQ_PROCESS_TRACK_STEPS_NUM = "YQ_PROCESS_TRACK_STEPS_NUM";

}
