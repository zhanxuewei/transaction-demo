/**
 *
 */
package com.mk.generate.common.api;

import lombok.Data;

import java.util.Map;

/**
 * access token
 */
@Data
public class TokenInfo {

    /**
     * token 值
     */
    private String access_token;

    /**
     * token 类型
     */
    private String token_type;

    /**
     * 刷新 token
     */
    private String refresh_token;

    /**
     * 作用域
     */
    private String scope;


    /**
     * 额外数据
     */
    private Map<String, Object> additionalInfo;

}
