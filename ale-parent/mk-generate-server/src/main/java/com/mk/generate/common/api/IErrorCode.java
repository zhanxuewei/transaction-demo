package com.mk.generate.common.api;

/**
 * 封装API的错误码
 */
public interface IErrorCode {

    /**
     * 错误码
     *
     * @return long
     */
    long getCode();

    /**
     * 错误消息
     *
     * @return string
     */
    String getMessage();

}
