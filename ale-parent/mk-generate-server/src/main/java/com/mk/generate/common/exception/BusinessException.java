package com.mk.generate.common.exception;


/**
 * 业务异常处理
 */
public class BusinessException extends Exception {

    public BusinessException() {
        super();
    }

    public BusinessException(String message) {
        super(message);
    }

}
