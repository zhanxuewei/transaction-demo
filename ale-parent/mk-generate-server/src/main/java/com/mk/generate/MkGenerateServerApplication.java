package com.mk.generate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MkGenerateServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MkGenerateServerApplication.class, args);
	}

}
