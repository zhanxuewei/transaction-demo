package com.mk.generate.common.api;


/**
 * 枚举了一些常用API操作码
 */
public enum ResultCode implements IErrorCode {
    /**
     *
     */
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录"),
    USE_NOT_DELETE(402, "被使用不能删除"),
    NOT_UPDATE(403, "不能修改"),
    AUTHORIZATION_HEADER_IS_EMPTY(600, "请求头中的token为空"),
    GET_TOKEN_KEY_ERROR(601, "远程获取TokenKey异常"),
    GEN_PUBLIC_KEY_ERROR(602, "生成公钥异常"),
    JWT_TOKEN_EXPIRE(603, "token校验异常或token已经过期"),
    TO_MANY_REQUEST_ERROR(429, "后端服务触发流控"),
    BACKGROUND_DEGRADE_ERROR(604, "后端服务触发降级"),
    BAD_GATEWAY(502, "网关服务异常"),
    FILE_UPLOAD_NOT_NULL(10001, "上传文件不能为空"),
    FORBIDDEN(403, "没有相关权限");

    /**
     * code
     */
    private final long code;
    /**
     * message
     */
    private final String message;

    private ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getAll(){
        return code+"|"+message;
    }
}
