package com.mk.generate.common.exception;


import com.mk.generate.common.api.IErrorCode;
import lombok.Data;

/**
 * 网关异常类
 */
@Data
public class GateWayException extends RuntimeException {

    private long code;

    private String message;

    public GateWayException(IErrorCode iErrorCode) {
        this.code = iErrorCode.getCode();
        this.message = iErrorCode.getMessage();
    }
}
