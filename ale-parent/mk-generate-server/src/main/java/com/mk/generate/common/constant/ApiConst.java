package com.mk.generate.common.constant;

/**
 * 接口常量
 */
public class ApiConst {

    /**
     * api 前缀
     */
    public static final String API_PREFIX = "/api/v1";

}
