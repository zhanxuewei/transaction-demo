package com.mk.generate.common.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.util.List;

/**
 * 分页数据封装类
 * @param <T>
 */
@Data
public class CommonPage<T> {

    /**
     * 当前页
     */
    private Long current;

    /**
     * 每页显示条数
     */
    private Long size;

    /**
     * 总页数
     */
    private Long total;

    /**
     * 数据
     */
    private List<T> list;

    /**
     * 将分页后的 page 转为分页信息
     *
     * @param page IPage
     * @param <T>  Object Type
     * @return CommonPage
     */
    public static <T> CommonPage<T> restPage(IPage<T> page) {
        CommonPage<T> result = new CommonPage<>();
        result.setCurrent(page.getCurrent());
        result.setSize(page.getSize());
        result.setTotal(page.getTotal());
        result.setList(page.getRecords());
        return result;
    }


    /**
     * @param page IPage
     * @param list 分页数据
     * @param <T>  Object Type
     * @return CommonPage
     */
    public static <T> CommonPage<T> restPage(IPage page, List<T> list) {
        CommonPage<T> result = new CommonPage<>();
        result.setCurrent(page.getCurrent());
        result.setSize(page.getSize());
        result.setTotal(page.getTotal());
        result.setList(list);
        return result;
    }
}
