package com.mk.generate;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.*;

/**
 * @description:
 * @author: zengweixiong
 * @createDate: 2021/12/4 20:20
 * @version: 1.0
 */
public class GeneratorApp {

    /**
     * 项目名称
     */
    private static final String PROJECT_NAME = "ale";

    /**
     * author
     */
    private static final String AUTHOR = "zhanxuewei";

    /**
     * 项目一级包名
     */
    private static final String PACKAGE = "com.ale.order";


    public static void main(String[] args) {
//        String moduleName = scanner("模块名");
        String tableName = scanner("表名 不支持多张表");
        String prefixName = scanner("需要替换的表前缀");
        String towPackage = scanner("VO, xxxQuery所在包名 如: user");
        String flag = scanner("选择生成的类型: 1 Controller; 2 Service; 3 Mapper; 4 Entity");

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setUrl("jdbc:mysql://192.168.110.18:3306/thg_management?characterEncoding=utf8&useSSL=false&serverTimezne=UTC&");
        dsc.setUrl("jdbc:mysql://120.79.12.129:3306/ale_order?characterEncoding=utf8&useSSL=false&serverTimezne=UTC&");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("zhanxuewei123..");

        // 获得当前项目的路径
        String projectPath = System.getProperty("user.dir") + "/" + PROJECT_NAME;

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        // 设置生成路径
        gc.setOutputDir(projectPath + "/src/main/java");
        //作者
        gc.setAuthor(AUTHOR);
        // 代码生成是不是要打开所在文件夹
        gc.setOpen(false);
        // 生成Swagger2注解
        gc.setSwagger2(false);
        // 会在mapper.xml 生成一个基础的<ResultMap> 映射所有的字段
        gc.setBaseResultMap(true);
        // 同文件生成覆盖
        gc.setFileOverride(true);
        // 时间类型对应策略
        gc.setDateType(DateType.ONLY_DATE);
        gc.setIdType(IdType.ASSIGN_ID);

        // 包配置
        PackageConfig pc = new PackageConfig();
        // 模块名
//        pc.setModuleName(moduleName);
        // 包名
        pc.setParent(PACKAGE);
        // 设置entity包名
        pc.setEntity("entity." + towPackage);

        if ("1".equals(flag)) {
            // 生成 controller service mapper xml entity
            all(pc, projectPath, tableName, prefixName, dsc, gc);
            // 生成 VO
            dataObject("VO", pc, projectPath, tableName, prefixName, dsc, gc);
            // 生成 xxQuery
            dataObject("Query", pc, projectPath, tableName, prefixName, dsc, gc);
            // 生成 xxListQuery
            listQuery("ListQuery", pc, projectPath, tableName, prefixName, dsc, gc);
        }
        if ("2".equals(flag)) {
            // 生成 service mapper xml entity
            service(pc, projectPath, tableName, prefixName, dsc, gc);
        }
        if ("3".equals(flag)) {
            // 生成 service mapper xml entity
            mapper(pc, projectPath, tableName, prefixName, dsc, gc);
        }
        if ("4".equals(flag)) {
            // 生成  entity
            entity(pc, projectPath, tableName, prefixName, dsc, gc);
        }

    }

    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + ":");
        // 判断用户是否输入
        if (scanner.hasNext()) {
            // 拿到输入内容
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "!");
    }

    private static void dataObject(String dataType, PackageConfig pc, String projectPath, String tableName, String prefixName,
                                   DataSourceConfig dsc, GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%s" + dataType);

        // 将全局配置设置到AutoGenerator
        // 生成Swagger2注解
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();

        tc.setEntity("/templates/data-object.java.vm");

        // 禁用模板
        tc.disable(TemplateType.CONTROLLER, TemplateType.SERVICE, TemplateType.XML, TemplateType.MAPPER);

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }

    private static void listQuery(String dataType, PackageConfig pc, String projectPath, String tableName, String prefixName,
                                   DataSourceConfig dsc, GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%s" + dataType);

        // 将全局配置设置到AutoGenerator
        // 生成Swagger2注解
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();

        tc.setEntity("/templates/list-query.java.vm");

        // 禁用模板
        tc.disable(TemplateType.CONTROLLER, TemplateType.SERVICE, TemplateType.XML, TemplateType.MAPPER);

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }

    private static void entity(PackageConfig pc, String projectPath, String tableName, String prefixName,
                               DataSourceConfig dsc, GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%sEntity");

        // 将全局配置设置到AutoGenerator
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();
        // controller
        tc.setEntity("/templates/entity.java.vm");

        // 禁用模板
        tc.disable(TemplateType.CONTROLLER, TemplateType.SERVICE, TemplateType.XML, TemplateType.MAPPER);

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }

    private static void mapper(PackageConfig pc, String projectPath, String tableName, String prefixName,
                               DataSourceConfig dsc, GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%sEntity");
        // mapper接口名
        gc.setMapperName("%sMapper");
        // mapper.xml 文件名
        gc.setXmlName("%sMapper");

        // 将全局配置设置到AutoGenerator
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        pc.setMapper("mapper");
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化!!
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName().replace("Entity", "") + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();
        // controller
        tc.setXml("/templates/mapper.xml.vm");
        tc.setMapper("/templates/mapper.java.vm");
        tc.setEntity("/templates/entity.java.vm");

        // 禁用模板
        tc.disable(TemplateType.CONTROLLER, TemplateType.SERVICE);

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }

    private static void service(PackageConfig pc, String projectPath, String tableName, String prefixName, DataSourceConfig dsc
            , GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%sEntity");
        // mapper接口名
        gc.setMapperName("%sMapper");
        // mapper.xml 文件名
        gc.setXmlName("%sMapper");

        // 业务逻辑类接口名
        gc.setServiceName("%sService");
        // 业务逻辑类实现类名
        gc.setServiceImplName("%sServiceImpl");
        // 将全局配置设置到AutoGenerator
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        pc.setMapper("mapper");
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化!!
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName().replace("Entity", "") + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();
        // controller
        tc.setService("/templates/service.java.vm");
        tc.setServiceImpl("/templates/serviceImpl.java.vm");
        tc.setXml("/templates/mapper.xml.vm");
        tc.setMapper("/templates/mapper.java.vm");
        tc.setEntity("/templates/entity.java.vm");

        // 禁用模板
        tc.disable(TemplateType.CONTROLLER);

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }

    private static void all(PackageConfig pc, String projectPath, String tableName, String prefixName, DataSourceConfig dsc
            , GlobalConfig gc) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        mpg.setDataSource(dsc);

        // 实体名:直接用表名 %s=表名
        gc.setEntityName("%sEntity");
        // mapper接口名
        gc.setMapperName("%sMapper");
        // mapper.xml 文件名
        gc.setXmlName("%sMapper");

        // 业务逻辑类接口名
        gc.setServiceName("%sService");
        // 业务逻辑类实现类名
        gc.setServiceImplName("%sServiceImpl");
        // 将全局配置设置到AutoGenerator
        mpg.setGlobalConfig(gc);

        // 完整的报名: com.example.demo1.pms
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                // this.getConfig().getGlobalConfig().getAuthor()
                // String entityPackage = this.getConfig().getPackageInfo().get("Entity");
                // String myPackage = entityPackage.substring(0, entityPackage.lastIndexOf(".", entityPackage.lastIndexOf(".") - 1));
                TableInfo tableInfo = this.getConfig().getTableInfoList().get(0);
                map.put("myPackage", PACKAGE);
                String entityName = tableInfo.getEntityName().replace("Entity", "");
                map.put("myName", entityName);
                map.put("myLowerCaseName", entityName.substring(0, 1).toLowerCase() + entityName.substring(1));
                this.setMap(map);
            }
        };
        pc.setMapper("mapper");
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化!!
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName().replace("Entity", "") + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig tc = new TemplateConfig();
        // controller
        tc.setController("/templates/controller.java.vm");
        tc.setService("/templates/service.java.vm");
        tc.setServiceImpl("/templates/serviceImpl.java.vm");
        tc.setXml("/templates/mapper.xml.vm");
        tc.setMapper("/templates/mapper.java.vm");
        tc.setEntity("/templates/entity.java.vm");

        // 把已有的xml生成置空
        tc.setXml(null);

        mpg.setTemplate(tc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 表名的生成策略:下划线转驼峰 pms_product ‐‐ PmsProduct
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 列名的生成策略:下划线转驼峰 last_name ‐‐ lastName
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        //sc.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        sc.setEntityLombokModel(true);
        // 在controller类上是否生成@RestController
        sc.setRestControllerStyle(true);
        // 乐观锁属性名设置
        sc.setVersionFieldName("version");
        // 逻辑删除属性名设置
        sc.setLogicDeleteFieldName("status");
        // 公共父类
        //sc.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");

        if (tableName.indexOf('*') > 0) {
            // 按前缀生成表
            sc.setLikeTable(new LikeTable(tableName.replace('*', '_')));
        } else {
            // 要生成的表名 多个用逗号分隔
            sc.setInclude(tableName);
        }
        // 设置表替换前缀
        sc.setTablePrefix(prefixName);
        // 属性前缀替换
//        sc.setFieldPrefix("is_");
        // 驼峰转连字符 比如 pms_product ‐‐> controller @RequestMapping("/pms/pmsProduct")
        //sc.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(sc);

        // 进行生成
        mpg.execute();
    }


}
