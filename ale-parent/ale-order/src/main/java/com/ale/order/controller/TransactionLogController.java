package com.ale.order.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.order.TransactionLogEntity;
import com.ale.common.entity.order.TransactionLogListQuery;
import com.ale.common.entity.order.TransactionLogQuery;
import com.ale.common.entity.order.TransactionLogVO;
import com.ale.common.enums.ResultCode;
import com.ale.order.service.TransactionLogService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 事务日志接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
@Api(tags = "事务日志接口", description = "TransactionLogController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/order/transaction")
public class TransactionLogController {

    @Autowired
    private TransactionLogService transactionLogService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody TransactionLogQuery transactionLogQuery) {
        transactionLogService.save(transactionLogQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<TransactionLogVO>> list(TransactionLogListQuery transactionLogListQuery) {
        IPage<TransactionLogEntity> page = transactionLogService.list(transactionLogListQuery);
        List<TransactionLogVO> list = page.getRecords().stream().map(item -> {
            TransactionLogVO transactionLogVO = new TransactionLogVO();
            BeanUtils.copyProperties(item, transactionLogVO);
            return transactionLogVO;
        }).collect(Collectors.toList());
        return ApiResult.success(PageResult.restPage(page, list));
    }

    @ApiOperation("批量添加")
    @PostMapping("/saveBatch")
    public ApiResult<ResultCode> saveBatch(@RequestBody List<TransactionLogQuery> list) {
        transactionLogService.saveBatch(list);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody TransactionLogQuery transactionLogQuery) {
        transactionLogService.updateById(transactionLogQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<String> ids) {
        transactionLogService.removeByIds(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    public ApiResult<TransactionLogVO> detail(@PathVariable("id") String id) {
        TransactionLogEntity transactionLogEntity = transactionLogService.getById(id);
        TransactionLogVO transactionLogVO = new TransactionLogVO();
        BeanUtils.copyProperties(transactionLogEntity, transactionLogVO);
        return ApiResult.success(transactionLogVO);
    }

}

