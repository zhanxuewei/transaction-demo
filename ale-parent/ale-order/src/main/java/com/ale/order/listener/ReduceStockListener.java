package com.ale.order.listener;

import com.ale.common.entity.order.TransactionLogEntity;
import com.ale.common.message.MqReduceStockDTO;
import com.ale.order.service.OrderService;
import com.ale.order.service.TransactionLogService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.util.Objects;

@Slf4j
@RocketMQTransactionListener
public class ReduceStockListener implements RocketMQLocalTransactionListener {

    @Autowired
    private OrderService orderService;

    @Autowired
    private TransactionLogService transactionLogService;

    /**
     * 执行本地事务
     *
     * @param message
     * @param o
     * @return
     */
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        log.info("执行本地事务");
        MessageHeaders headers = message.getHeaders();
        String transactionId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
        Long userId = Long.valueOf((String) headers.get("userId"));
        String json = new String((byte[]) message.getPayload());
        MqReduceStockDTO mqReduceStockDTO = JSON.parseObject(json, MqReduceStockDTO.class);
        log.info("事务ID：{}", transactionId);
        try {
            // 执行本地事务
            orderService.saveOrderWithTransactionLog(transactionId, mqReduceStockDTO.getUserVO(), mqReduceStockDTO.getProductItemDTOList());
            log.info("本地事务执行成功");
            return RocketMQLocalTransactionState.COMMIT;
        } catch (Exception e) {
            log.info("本地事务执行失败");
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }


    /**
     * 本地事务回查
     *
     * @param message
     * @return
     */
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        MessageHeaders headers = message.getHeaders();
        String transactionId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
        log.info("检查本地事务，事务ID：{}", transactionId);
        LambdaQueryWrapper<TransactionLogEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TransactionLogEntity::getTransactionId, transactionId);
        TransactionLogEntity transactionLogEntity = transactionLogService.getOne(wrapper);
        if (Objects.nonNull(transactionLogEntity)) {
            return RocketMQLocalTransactionState.COMMIT;
        }
        return RocketMQLocalTransactionState.ROLLBACK;

    }

}
