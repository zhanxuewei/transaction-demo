package com.ale.order.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ale.common.api.ApiResult;
import com.ale.common.entity.order.*;
import com.ale.common.entity.product.ProductVO;
import com.ale.common.entity.user.UserVO;
import com.ale.common.enums.ResultCode;
import com.ale.common.exception.BizException;
import com.ale.common.feign.dto.product.ProductItemDTO;
import com.ale.common.feign.dto.product.ProductItemInput;
import com.ale.common.feign.dto.stock.StockDTO;
import com.ale.common.mapper.order.OrderMapper;
import com.ale.common.message.MqReduceStockDTO;
import com.ale.order.feign.ProductServerFeignApi;
import com.ale.order.feign.StockServerFeignApi;
import com.ale.order.feign.UserServerFeignApi;
import com.ale.order.service.OrderProductService;
import com.ale.order.service.OrderService;
import com.ale.order.service.OrderUserService;
import com.ale.order.service.TransactionLogService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单主表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderEntity> implements OrderService {

    @Autowired
    private StockServerFeignApi stockServerFeignApi;

    @Autowired
    private UserServerFeignApi userServerFeignApi;

    @Autowired
    private ProductServerFeignApi productServerFeignApi;

    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private OrderUserService orderUserService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private TransactionLogService transactionLogService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void create(OrderCreateDTO createDTO) {
        Long userId = createDTO.getUserId();
        List<ProductItemInput> productItemInputs = createDTO.getProductItemInputs();
        // 用户是否存在
        ApiResult<UserVO> userVOApiResult = userServerFeignApi.detail(userId);
        if (ResultCode.SUCCESS.getCode() != userVOApiResult.getCode()) {
            throw new BizException(userVOApiResult.getMessage(), userVOApiResult.getCode());
        }
        UserVO userVO = userVOApiResult.getData();

        // 商品是否存在
        ApiResult<List<ProductVO>> listApiResult = productServerFeignApi.queryByIds(productItemInputs.stream().map(ProductItemInput::getProductId).collect(Collectors.toList()));
        if (ResultCode.SUCCESS.getCode() != listApiResult.getCode()) {
            throw new BizException(listApiResult.getMessage(), listApiResult.getCode());
        }
        List<ProductVO> productVOList = listApiResult.getData();
        if (CollUtil.isEmpty(productVOList) || productVOList.size() != productItemInputs.size()) {
            throw new BizException(ResultCode.PRODUCT_NOT_FOUND);
        }

//        for (int i = 0; i < productVOList.size(); i++) {
//            ProductVO productVO = productVOList.get(i);
//            int number = productItems.get(i).getNumber();
//            BigDecimal amount = productVO.getProductPrice().multiply(new BigDecimal(number)).setScale(2, BigDecimal.ROUND_UP);
//
//            // 2.生成订单主表信息
//            OrderEntity orderEntity = new OrderEntity();
//            orderEntity.setId(null);
//            orderEntity.setOrderNumber(createOrderNumber());
//            orderEntity.setOrderAmount(amount);
//            orderEntity.setOrderPayStatus(0);
//            orderEntity.setOrderPayType(null);
//            orderEntity.setOrderPayTime(null);
//            orderEntity.setOrderStatus(1);
//            super.save(orderEntity);
//
//            // 3.生成订单商品信息
//            OrderProductEntity orderProductEntity = new OrderProductEntity();
//            orderProductEntity.setId(null);
//            orderProductEntity.setOrderId(orderEntity.getId());
//            orderProductEntity.setProductId(productVO.getId());
//            orderProductEntity.setProductName(productVO.getProductName());
//            orderProductEntity.setProductCategoryId(productVO.getProductCategoryId());
//            orderProductEntity.setProductCategoryName(productVO.getProductCategoryName());
//            orderProductEntity.setProductQuantity(number);
//            orderProductEntity.setProductPrice(productVO.getProductPrice());
//            orderProductService.save(orderProductEntity);
//
//            // 4.生成订单用户信息
//            OrderUserEntity orderUserEntity = new OrderUserEntity();
//            orderUserEntity.setId(null);
//            orderUserEntity.setOrderId(orderEntity.getId());
//            orderUserEntity.setUserId(userVO.getId());
//            orderUserEntity.setName(userVO.getName());
//            orderUserEntity.setSex(userVO.getSex());
//            orderUserEntity.setPhone(userVO.getPhone());
//            orderUserService.save(orderUserEntity);
//        }

        // 1.扣库存 TODO
//        List<StockDTO> stockDTOList = productItems.stream().map(item -> {
//            StockDTO stockDTO = new StockDTO();
//            stockDTO.setProductId(item.getProductId());
//            stockDTO.setNumber(item.getNumber());
//            return stockDTO;
//        }).collect(Collectors.toList());
//        ApiResult<ResultCode> stockReduceApiResult = this.stockServerFeignApi.reduce(stockDTOList);
//        if (ResultCode.SUCCESS.getCode() != stockReduceApiResult.getCode()) {
//            throw new BizException(stockReduceApiResult.getMessage(), stockReduceApiResult.getCode());
//        }
        List<ProductItemDTO> productItemDTOList = new ArrayList<>();
        for (int i = 0; i < productItemInputs.size(); i++) {
            ProductItemDTO dto = new ProductItemDTO();
            BeanUtils.copyProperties(productVOList.get(i), dto);
            dto.setNumber(productItemInputs.get(i).getNumber());
            productItemDTOList.add(dto);
        }


        // TODO 1.校验 2.生成transactionId,发送half消息
        String transactionId = UUID.randomUUID().toString();
        MqReduceStockDTO mqReduceStockDTO = new MqReduceStockDTO();
        mqReduceStockDTO.setTransactionId(transactionId);
        mqReduceStockDTO.setProductItemDTOList(productItemDTOList);
        mqReduceStockDTO.setUserVO(userVO);
        mqReduceStockDTO.setSex(createDTO.getSex());
        TransactionSendResult result = rocketMQTemplate.sendMessageInTransaction("reduce-stock",
                MessageBuilder.withPayload(JSON.toJSONString(mqReduceStockDTO))
                        .setHeader("userId", userId)
                        .setHeader(RocketMQHeaders.TRANSACTION_ID, transactionId)
                        .build(),
                transactionId);
        log.error("本地事务执行结果 = {}", result);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void cancel(Long orderId) {
        // 删除订单主表信息
        OrderEntity orderEntity = super.getById(orderId);
        if (Objects.isNull(orderEntity)) {
            throw new BizException(ResultCode.ORDER_NOT_FOUND);
        }

        // 订单商品信息
        List<OrderProductEntity> orderProductEntityList = orderProductService.list(new LambdaQueryWrapper<OrderProductEntity>()
                .eq(OrderProductEntity::getOrderId, orderEntity.getId()));
        if (CollUtil.isEmpty(orderProductEntityList)) {
            throw new BizException(ResultCode.ORDER_PRODUCT_NOT_FOUND);
        }

        // 订单用户信息
        List<OrderUserEntity> orderUserEntityList = orderUserService.list(new LambdaQueryWrapper<OrderUserEntity>()
                .eq(OrderUserEntity::getOrderId, orderEntity.getId()));
        if (CollUtil.isEmpty(orderUserEntityList)) {
            throw new BizException(ResultCode.ORDER_USER_NOT_FOUND);
        }

        // 订单设置为已取消
        orderEntity.setOrderStatus(0);
        super.updateById(orderEntity);

        // 库存增加
        List<StockDTO> stockDTOList = orderProductEntityList.stream().map(item -> {
            StockDTO stockDTO = new StockDTO();
            stockDTO.setProductId(item.getProductId());
            stockDTO.setNumber(item.getProductQuantity());
            return stockDTO;
        }).collect(Collectors.toList());
        ApiResult<ResultCode> apiResult = stockServerFeignApi.increase(stockDTOList);
        if (ResultCode.SUCCESS.getCode() != apiResult.getCode()) {
            throw new BizException(apiResult.getMessage(), apiResult.getCode());
        }
    }


    @Override
    public void saveOrderWithTransactionLog(String transactionId, UserVO userVO, List<ProductItemDTO> productItemDTOList) {
        BigDecimal amount = BigDecimal.ZERO;
        // 计算订单总金额
        for (ProductItemDTO productItemDTO : productItemDTOList) {
            BigDecimal temp = productItemDTO.getProductPrice().multiply(new BigDecimal(productItemDTO.getNumber())).setScale(2, BigDecimal.ROUND_UP);
            amount = amount.add(temp);
        }

        // 2.生成订单主表信息
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(null);
        orderEntity.setOrderNumber(createOrderNumber());
        orderEntity.setOrderAmount(amount);
        orderEntity.setOrderPayStatus(0);
        orderEntity.setOrderPayType(null);
        orderEntity.setOrderPayTime(null);
        orderEntity.setOrderStatus(1);
        super.save(orderEntity);

        // 4.生成订单用户信息
        OrderUserEntity orderUserEntity = new OrderUserEntity();
        orderUserEntity.setId(null);
        orderUserEntity.setOrderId(orderEntity.getId());
        orderUserEntity.setUserId(userVO.getId());
        orderUserEntity.setName(userVO.getName());
        orderUserEntity.setSex(userVO.getSex());
        orderUserEntity.setPhone(userVO.getPhone());
        orderUserService.save(orderUserEntity);

        for (ProductItemDTO productItemDTO : productItemDTOList) {
            // 3.生成订单商品信息
            OrderProductEntity orderProductEntity = new OrderProductEntity();
            orderProductEntity.setId(null);
            orderProductEntity.setOrderId(orderEntity.getId());
            orderProductEntity.setProductId(productItemDTO.getId());
            orderProductEntity.setProductName(productItemDTO.getProductName());
            orderProductEntity.setProductCategoryId(productItemDTO.getProductCategoryId());
            orderProductEntity.setProductCategoryName(productItemDTO.getProductCategoryName());
            orderProductEntity.setProductQuantity(productItemDTO.getNumber());
            orderProductEntity.setProductPrice(productItemDTO.getProductPrice());
            orderProductService.save(orderProductEntity);
        }

        TransactionLogEntity transactionLogEntity = new TransactionLogEntity();
        transactionLogEntity.setTransactionId(transactionId);
        transactionLogEntity.setLog("执行创建订单操作");
        transactionLogEntity.setOrderId(orderEntity.getId());
        transactionLogService.save(transactionLogEntity);
    }

    /**
     * 生成订单编号
     *
     * @return 订单编号
     */
    private String createOrderNumber() {
        return RandomStringUtils.randomAlphanumeric(32);
    }

}
