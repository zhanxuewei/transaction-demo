package com.ale.order.service.impl;

import com.ale.common.entity.order.OrderUserEntity;
import com.ale.common.entity.order.OrderUserListQuery;
import com.ale.common.entity.order.OrderUserQuery;
import com.ale.common.mapper.order.OrderUserMapper;
import com.ale.order.service.OrderUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单用户信息表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class OrderUserServiceImpl extends ServiceImpl<OrderUserMapper, OrderUserEntity> implements OrderUserService {


    /**
     * 添加
     *
     * @param orderUserQuery OrderUserQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean save(OrderUserQuery orderUserQuery) {
        OrderUserEntity orderUserEntity = new OrderUserEntity();
        BeanUtils.copyProperties(orderUserQuery, orderUserEntity);
        return super.save(orderUserEntity);
    }

    /**
     * 批量添加
     *
     * @param list List<OrderUserQuery>
     * @return 是否成功 true|false
     */
    @Override
    public boolean saveBatch(List<OrderUserQuery> list) {
        List<OrderUserEntity> collect = list.stream().map(item -> {
            OrderUserEntity orderUserEntity = new OrderUserEntity();
            BeanUtils.copyProperties(item, orderUserEntity);
            return orderUserEntity;
        }).collect(Collectors.toList());
        return super.saveBatch(collect);
    }

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean removeByIds(List<String> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 根据ID修改
     *
     * @param orderUserQuery OrderUserQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean updateById(OrderUserQuery orderUserQuery) {
        OrderUserEntity orderUserEntity = new OrderUserEntity();
        BeanUtils.copyProperties(orderUserQuery, orderUserEntity);
        return super.updateById(orderUserEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return OrderUserDO
     */
    @Override
    public OrderUserEntity getById(String id) {
        return super.getById(id);
    }

    /**
     * 条件分页查询
     *
     * @param orderUserListQuery OrderUserListQuery
     * @return IPage<OrderUserDO>
     */
    @Override
    public IPage<OrderUserEntity> list(OrderUserListQuery orderUserListQuery) {
        IPage<OrderUserEntity> pageInfo = new Page<>(orderUserListQuery.getCurrent(), orderUserListQuery.getSize());
        QueryWrapper<OrderUserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(OrderUserEntity::getStatus, 1)
                .orderByDesc(OrderUserEntity::getCreateTime);
        return super.page(pageInfo, queryWrapper);
    }

}
