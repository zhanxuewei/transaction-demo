package com.ale.order.service;


import com.ale.common.entity.order.OrderProductEntity;
import com.ale.common.entity.order.OrderProductListQuery;
import com.ale.common.entity.order.OrderProductQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单商品信息表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderProductService extends IService<OrderProductEntity> {

    /**
     * 添加
     *
     * @param orderProductQuery OrderProductQuery
     * @return 是否成功 true|false
     */
    boolean save(OrderProductQuery orderProductQuery);

    /**
     * 批量添加
     *
     * @param list List<OrderProductQuery>
     * @return 是否成功 true|false
     */
    boolean saveBatch(List<OrderProductQuery> list);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<String> ids);

    /**
     * 根据ID修改
     *
     * @param orderProductQuery OrderProductQuery
     * @return 是否成功 true|false
     */
    boolean updateById(OrderProductQuery orderProductQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return OrderProductDO
     */
    OrderProductEntity getById(String id);

    /**
     * 条件分页查询
     *
     * @param orderProductListQuery OrderProductListQuery
     * @return IPage<OrderProductDO>
     */
    IPage<OrderProductEntity> list(OrderProductListQuery orderProductListQuery);

}
