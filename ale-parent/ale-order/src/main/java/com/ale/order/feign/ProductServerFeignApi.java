package com.ale.order.feign;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.product.ProductVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * feign客户端
 */
@Component
@FeignClient(name = "ale-product-server", path = ApiConst.API_PREFIX + "/product")
public interface ProductServerFeignApi {

    /**
     * 根据id集合查询
     *
     * @param ids
     * @return
     */
    @PostMapping("/queryByIds")
    ApiResult<List<ProductVO>> queryByIds(@RequestBody List<Long> ids);
}
