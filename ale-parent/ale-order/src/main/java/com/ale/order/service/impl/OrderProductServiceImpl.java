package com.ale.order.service.impl;

import com.ale.common.entity.order.OrderProductEntity;
import com.ale.common.entity.order.OrderProductListQuery;
import com.ale.common.entity.order.OrderProductQuery;
import com.ale.common.mapper.order.OrderProductMapper;
import com.ale.order.service.OrderProductService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单商品信息表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class OrderProductServiceImpl extends ServiceImpl<OrderProductMapper, OrderProductEntity> implements OrderProductService {


    /**
     * 添加
     *
     * @param orderProductQuery OrderProductQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean save(OrderProductQuery orderProductQuery) {
        OrderProductEntity orderProductEntity = new OrderProductEntity();
        BeanUtils.copyProperties(orderProductQuery, orderProductEntity);
        return super.save(orderProductEntity);
    }

    /**
     * 批量添加
     *
     * @param list List<OrderProductQuery>
     * @return 是否成功 true|false
     */
    @Override
    public boolean saveBatch(List<OrderProductQuery> list) {
        List<OrderProductEntity> collect = list.stream().map(item -> {
            OrderProductEntity orderProductEntity = new OrderProductEntity();
            BeanUtils.copyProperties(item, orderProductEntity);
            return orderProductEntity;
        }).collect(Collectors.toList());
        return super.saveBatch(collect);
    }

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean removeByIds(List<String> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 根据ID修改
     *
     * @param orderProductQuery OrderProductQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean updateById(OrderProductQuery orderProductQuery) {
        OrderProductEntity orderProductEntity = new OrderProductEntity();
        BeanUtils.copyProperties(orderProductQuery, orderProductEntity);
        return super.updateById(orderProductEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return OrderProductDO
     */
    @Override
    public OrderProductEntity getById(String id) {
        return super.getById(id);
    }

    /**
     * 条件分页查询
     *
     * @param orderProductListQuery OrderProductListQuery
     * @return IPage<OrderProductDO>
     */
    @Override
    public IPage<OrderProductEntity> list(OrderProductListQuery orderProductListQuery) {
        IPage<OrderProductEntity> pageInfo = new Page<>(orderProductListQuery.getCurrent(), orderProductListQuery.getSize());
        QueryWrapper<OrderProductEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(OrderProductEntity::getStatus, 1)
                .orderByDesc(OrderProductEntity::getCreateTime);
        return super.page(pageInfo, queryWrapper);
    }

}
