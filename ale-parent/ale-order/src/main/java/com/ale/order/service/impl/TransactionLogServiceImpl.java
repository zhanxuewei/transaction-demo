package com.ale.order.service.impl;

import com.ale.common.entity.order.TransactionLogEntity;
import com.ale.common.entity.order.TransactionLogListQuery;
import com.ale.common.entity.order.TransactionLogQuery;
import com.ale.common.mapper.order.TransactionLogMapper;
import com.ale.order.service.TransactionLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 事务日志 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
@Service
public class TransactionLogServiceImpl extends ServiceImpl<TransactionLogMapper, TransactionLogEntity> implements TransactionLogService {


    /**
     * 添加
     *
     * @param transactionLogQuery TransactionLogQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean save(TransactionLogQuery transactionLogQuery) {
        TransactionLogEntity transactionLogEntity = new TransactionLogEntity();
        BeanUtils.copyProperties(transactionLogQuery, transactionLogEntity);
        return super.save(transactionLogEntity);
    }

    /**
     * 批量添加
     *
     * @param list List<TransactionLogQuery>
     * @return 是否成功 true|false
     */
    @Override
    public boolean saveBatch(List<TransactionLogQuery> list) {
        List<TransactionLogEntity> collect = list.stream().map(item -> {
            TransactionLogEntity transactionLogEntity = new TransactionLogEntity();
            BeanUtils.copyProperties(item, transactionLogEntity);
            return transactionLogEntity;
        }).collect(Collectors.toList());
        return super.saveBatch(collect);
    }

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean removeByIds(List<String> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 根据ID修改
     *
     * @param transactionLogQuery TransactionLogQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean updateById(TransactionLogQuery transactionLogQuery) {
        TransactionLogEntity transactionLogEntity = new TransactionLogEntity();
        BeanUtils.copyProperties(transactionLogQuery, transactionLogEntity);
        return super.updateById(transactionLogEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return TransactionLogDO
     */
    @Override
    public TransactionLogEntity getById(String id) {
        return super.getById(id);
    }

    /**
     * 条件分页查询
     *
     * @param transactionLogListQuery TransactionLogListQuery
     * @return IPage<TransactionLogDO>
     */
    @Override
    public IPage<TransactionLogEntity> list(TransactionLogListQuery transactionLogListQuery) {
        IPage<TransactionLogEntity> pageInfo = new Page<>(transactionLogListQuery.getCurrent(), transactionLogListQuery.getSize());
        QueryWrapper<TransactionLogEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(TransactionLogEntity::getStatus, 1)
                .orderByDesc(TransactionLogEntity::getCreateTime);
        return super.page(pageInfo, queryWrapper);
    }

}
