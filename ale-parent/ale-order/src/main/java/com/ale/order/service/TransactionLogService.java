package com.ale.order.service;


import com.ale.common.entity.order.TransactionLogEntity;
import com.ale.common.entity.order.TransactionLogListQuery;
import com.ale.common.entity.order.TransactionLogQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 事务日志 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-04
 */
public interface TransactionLogService extends IService<TransactionLogEntity> {

    /**
     * 添加
     *
     * @param transactionLogQuery TransactionLogQuery
     * @return 是否成功 true|false
     */
    boolean save(TransactionLogQuery transactionLogQuery);

    /**
     * 批量添加
     *
     * @param list List<TransactionLogQuery>
     * @return 是否成功 true|false
     */
    boolean saveBatch(List<TransactionLogQuery> list);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<String> ids);

    /**
     * 根据ID修改
     *
     * @param transactionLogQuery TransactionLogQuery
     * @return 是否成功 true|false
     */
    boolean updateById(TransactionLogQuery transactionLogQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return TransactionLogDO
     */
    TransactionLogEntity getById(String id);

    /**
     * 条件分页查询
     *
     * @param transactionLogListQuery TransactionLogListQuery
     * @return IPage<TransactionLogDO>
     */
    IPage<TransactionLogEntity> list(TransactionLogListQuery transactionLogListQuery);

}
