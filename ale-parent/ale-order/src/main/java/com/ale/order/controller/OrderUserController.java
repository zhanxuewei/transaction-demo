package com.ale.order.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.order.OrderUserEntity;
import com.ale.common.entity.order.OrderUserListQuery;
import com.ale.common.entity.order.OrderUserQuery;
import com.ale.common.entity.order.OrderUserVO;
import com.ale.common.enums.ResultCode;
import com.ale.order.service.OrderUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 订单用户信息接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "订单用户信息接口", description = "OrderUserController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/order/user")
public class OrderUserController {

    @Autowired
    private OrderUserService orderUserService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody OrderUserQuery orderUserQuery) {
        orderUserService.save(orderUserQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<OrderUserVO>> list(OrderUserListQuery orderUserListQuery) {
        IPage<OrderUserEntity> page = orderUserService.list(orderUserListQuery);
        List<OrderUserVO> list = page.getRecords().stream().map(item -> {
            OrderUserVO orderUserVO = new OrderUserVO();
            BeanUtils.copyProperties(item, orderUserVO);
            return orderUserVO;
        }).collect(Collectors.toList());
        return ApiResult.success(PageResult.restPage(page, list));
    }

    @ApiOperation("批量添加")
    @PostMapping("/saveBatch")
    public ApiResult<ResultCode> saveBatch(@RequestBody List<OrderUserQuery> list) {
        orderUserService.saveBatch(list);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody OrderUserQuery orderUserQuery) {
        orderUserService.updateById(orderUserQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<String> ids) {
        orderUserService.removeByIds(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    public ApiResult<OrderUserVO> detail(@PathVariable("id") String id) {
        OrderUserEntity orderUserEntity = orderUserService.getById(id);
        OrderUserVO orderUserVO = new OrderUserVO();
        BeanUtils.copyProperties(orderUserEntity, orderUserVO);
        return ApiResult.success(orderUserVO);
    }

}

