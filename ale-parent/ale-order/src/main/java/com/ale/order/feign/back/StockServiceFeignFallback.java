package com.ale.order.feign.back;

import com.ale.common.api.ApiResult;
import com.ale.common.enums.ResultCode;
import com.ale.common.feign.dto.stock.StockDTO;
import com.ale.order.feign.StockServerFeignApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class StockServiceFeignFallback implements FallbackFactory<StockServerFeignApi> {
    @Override
    public StockServerFeignApi create(Throwable throwable) {
        String msg = throwable == null ? "" : throwable.getMessage();
        if (!StringUtils.isEmpty(msg)) {
            log.error(msg);
        }
        return new StockServerFeignApi() {


            @Override
            public ApiResult<ResultCode> reduce(List<StockDTO> stockDTOList) {
                return ApiResult.failed(msg);
            }

            @Override
            public ApiResult<ResultCode> increase(List<StockDTO> stockDTOList) {
                return ApiResult.failed(msg);
            }
        };
    }
}
