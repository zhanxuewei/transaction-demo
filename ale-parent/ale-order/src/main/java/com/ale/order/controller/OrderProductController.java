package com.ale.order.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.order.OrderProductEntity;
import com.ale.common.entity.order.OrderProductListQuery;
import com.ale.common.entity.order.OrderProductQuery;
import com.ale.common.entity.order.OrderProductVO;
import com.ale.common.enums.ResultCode;
import com.ale.order.service.OrderProductService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 订单商品信息接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "订单商品信息接口", description = "OrderProductController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/order/product")
public class OrderProductController {

    @Autowired
    private OrderProductService orderProductService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody OrderProductQuery orderProductQuery) {
        orderProductService.save(orderProductQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/list")
    public ApiResult<PageResult<OrderProductVO>> list(OrderProductListQuery orderProductListQuery) {
        IPage<OrderProductEntity> page = orderProductService.list(orderProductListQuery);
        List<OrderProductVO> list = page.getRecords().stream().map(item -> {
            OrderProductVO orderProductVO = new OrderProductVO();
            BeanUtils.copyProperties(item, orderProductVO);
            return orderProductVO;
        }).collect(Collectors.toList());
        return ApiResult.success(PageResult.restPage(page, list));
    }

    @ApiOperation("批量添加")
    @PostMapping("/saveBatch")
    public ApiResult<ResultCode> saveBatch(@RequestBody List<OrderProductQuery> list) {
        orderProductService.saveBatch(list);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody OrderProductQuery orderProductQuery) {
        orderProductService.updateById(orderProductQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<String> ids) {
        orderProductService.removeByIds(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    public ApiResult<OrderProductVO> detail(@PathVariable("id") String id) {
        OrderProductEntity orderProductEntity = orderProductService.getById(id);
        OrderProductVO orderProductVO = new OrderProductVO();
        BeanUtils.copyProperties(orderProductEntity, orderProductVO);
        return ApiResult.success(orderProductVO);
    }

}

