package com.ale.order.feign.back;

import com.ale.common.api.ApiResult;
import com.ale.common.entity.product.ProductVO;
import com.ale.order.feign.ProductServerFeignApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class ProductServiceFeignFallback implements FallbackFactory<ProductServerFeignApi> {
    @Override
    public ProductServerFeignApi create(Throwable throwable) {
        String msg = throwable == null ? "" : throwable.getMessage();
        if (!StringUtils.isEmpty(msg)) {
            log.error(msg);
        }
        return new ProductServerFeignApi() {


            @Override
            public ApiResult<List<ProductVO>> queryByIds(List<Long> ids) {
                return ApiResult.failed(msg);
            }
        };
    }
}
