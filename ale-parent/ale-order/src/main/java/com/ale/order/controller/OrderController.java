package com.ale.order.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.order.*;
import com.ale.common.enums.ResultCode;
import com.ale.order.service.OrderService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 订单接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "订单接口", description = "OrderController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation("创建订单")
    @PostMapping("/create")
    public ApiResult<ResultCode> create(@RequestBody @Validated OrderCreateDTO createDTO) {
        orderService.create(createDTO);
        return ApiResult.success();
    }

    @ApiOperation("取消订单")
    @GetMapping("/cancel")
    public ApiResult<ResultCode> cancel(@RequestParam("orderId") Long orderId) {
        orderService.cancel(orderId);
        return ApiResult.success();
    }

}

