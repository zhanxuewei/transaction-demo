package com.ale.order.feign;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.enums.ResultCode;
import com.ale.common.feign.dto.stock.StockDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * feign客户端
 */
@Component
@FeignClient(name = "ale-stock-server", path = ApiConst.API_PREFIX + "/stock")
public interface StockServerFeignApi {

    /**
     * 扣减库存
     *
     * @param stockDTOList
     * @return
     */
    @PostMapping("/reduce")
    ApiResult<ResultCode> reduce(@RequestBody List<StockDTO> stockDTOList);

    /**
     * 增加库存
     *
     * @param stockDTOList
     * @return
     */
    @PostMapping("/increase")
    ApiResult<ResultCode> increase(@RequestBody List<StockDTO> stockDTOList);
}
