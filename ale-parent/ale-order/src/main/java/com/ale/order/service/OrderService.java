package com.ale.order.service;


import com.ale.common.entity.order.OrderCreateDTO;
import com.ale.common.entity.order.OrderEntity;
import com.ale.common.entity.product.ProductVO;
import com.ale.common.entity.user.UserVO;
import com.ale.common.feign.dto.product.ProductItemDTO;
import com.ale.common.feign.dto.product.ProductItemInput;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderService extends IService<OrderEntity> {

    /**
     * 创建订单
     * @param createDTO
     */
    void create(OrderCreateDTO createDTO);

    /**
     * 取消订单
     * @param orderId 订单id
     */
    void cancel(Long orderId);

    void saveOrderWithTransactionLog(String transactionId, UserVO userVO, List<ProductItemDTO> productItemDTOList);

}
