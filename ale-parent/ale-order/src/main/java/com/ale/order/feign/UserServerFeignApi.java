package com.ale.order.feign;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.user.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * feign客户端
 */
@Component
@FeignClient(name = "ale-user-server", path = ApiConst.API_PREFIX + "/user")
public interface UserServerFeignApi {

    /**
     * 用户详情
     *
     * @param id 用户id
     * @return 用户信息
     */
    @GetMapping("/detail")
    ApiResult<UserVO> detail(@RequestParam("id") Long id);
}
