package com.ale.order.service;


import com.ale.common.entity.order.OrderUserEntity;
import com.ale.common.entity.order.OrderUserListQuery;
import com.ale.common.entity.order.OrderUserQuery;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单用户信息表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface OrderUserService extends IService<OrderUserEntity> {

    /**
     * 添加
     *
     * @param orderUserQuery OrderUserQuery
     * @return 是否成功 true|false
     */
    boolean save(OrderUserQuery orderUserQuery);

    /**
     * 批量添加
     *
     * @param list List<OrderUserQuery>
     * @return 是否成功 true|false
     */
    boolean saveBatch(List<OrderUserQuery> list);

    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean removeByIds(List<String> ids);

    /**
     * 根据ID修改
     *
     * @param orderUserQuery OrderUserQuery
     * @return 是否成功 true|false
     */
    boolean updateById(OrderUserQuery orderUserQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return OrderUserDO
     */
    OrderUserEntity getById(String id);

    /**
     * 条件分页查询
     *
     * @param orderUserListQuery OrderUserListQuery
     * @return IPage<OrderUserDO>
     */
    IPage<OrderUserEntity> list(OrderUserListQuery orderUserListQuery);

}
