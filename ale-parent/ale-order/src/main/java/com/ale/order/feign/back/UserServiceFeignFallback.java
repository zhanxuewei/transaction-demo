package com.ale.order.feign.back;

import com.ale.common.api.ApiResult;
import com.ale.common.entity.user.UserVO;
import com.ale.order.feign.UserServerFeignApi;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserServiceFeignFallback implements FallbackFactory<UserServerFeignApi> {
    @Override
    public UserServerFeignApi create(Throwable throwable) {
        String msg = throwable == null ? "" : throwable.getMessage();
        if (!StringUtils.isEmpty(msg)) {
            log.error(msg);
        }
        return new UserServerFeignApi() {


            @Override
            public ApiResult<UserVO> detail(Long id) {
                return ApiResult.failed(msg);
            }
        };
    }
}
