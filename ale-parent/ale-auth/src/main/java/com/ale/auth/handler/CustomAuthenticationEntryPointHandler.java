package com.ale.auth.handler;

import com.ale.common.api.ApiResult;
import com.ale.common.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author admin
 * @description: 解决匿名用户访问无权限资源时的异常处理器
 * 实际上也就是用户没有认证过,请求头中没有携带了Token，或者是resource_ids范围不够
 */
@Component("customAuthenticationEntryPointHandler")
@Slf4j
public class CustomAuthenticationEntryPointHandler extends OAuth2AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.info("匿名用户访问无权限资源时的异常 errorMsg = {}", authException.getMessage());

        Throwable cause = authException.getCause();
        ApiResult<String> apiResult = resolveException(cause);

        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        PrintWriter printWriter = response.getWriter();
        printWriter.append(new ObjectMapper().writeValueAsString(apiResult));
    }

    private ApiResult<String> resolveException(Throwable cause) {
        if (cause instanceof OAuth2AccessDeniedException) {
            log.info("resource_ids范围不够");
            return ApiResult.failed("resource_ids范围不够");
        } else if (cause instanceof InvalidTokenException) {
            log.info("Token解析失败");
            return ApiResult.failed("Token解析失败");
        } else if (cause instanceof InsufficientAuthenticationException) {
            log.info("未携带token");
            return ApiResult.failed("未携带token");
        }
        return ApiResult.failed(ResultCode.FAILED);
    }
}
