package com.ale.auth.feign;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.user.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * feign客户端
 */
@Component
@FeignClient(name = "ale-user-server", path = ApiConst.API_PREFIX + "/user")
public interface UserServerFeignApi {

    /**
     * 根据用户名称查询
     *
     * @param name 用户名
     * @return
     */
    @GetMapping("/getUserByName")
    ApiResult<UserVO> getUserByName(@RequestParam("name") String name);

}
