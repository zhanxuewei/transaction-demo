package com.ale.auth.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.enums.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 认证服务接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2022-05-19
 */
@Api(tags = "认证服务接口", description = "AuthController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/auth")
@Slf4j
public class AuthController {


    @ApiOperation("测试接口")
    @GetMapping("/test")
    public ApiResult<String> save(@RequestParam("name") String name) {
        log.info("认证服务接口...");
        return ApiResult.success(ResultCode.SUCCESS, name);
    }
}

