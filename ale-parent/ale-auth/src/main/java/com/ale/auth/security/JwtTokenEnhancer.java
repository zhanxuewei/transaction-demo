package com.ale.auth.security;

import com.ale.auth.security.service.CustomerUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author admin
 * @version 1.0
 * @description: 认证成功后添加拓展信息
 * @date 2022/5/27 16:24
 */
@Component
@Slf4j
public class JwtTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        this.extendData(oAuth2AccessToken,oAuth2Authentication);
        return oAuth2AccessToken;
    }

    /**
     * 扩展信息
     * TODO 刷新令牌的时候获取到的Principal为登陆用户名，这里需要判断区分是"获取令牌"还是"刷新token"
     *
     * @param oAuth2AccessToken
     * @param oAuth2Authentication
     */
    public void extendData(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        CustomerUserDetails customerUserDetails = (CustomerUserDetails) oAuth2Authentication.getPrincipal();
        Map<String, Object> map = new HashMap<>();
        map.put("id", customerUserDetails.getUserVO().getId());
        map.put("name", customerUserDetails.getUserVO().getName());
        map.put("phone", customerUserDetails.getUserVO().getPhone());
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(map);
    }

}
