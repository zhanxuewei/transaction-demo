package com.ale.user.controller;

import com.ale.common.api.ApiResult;
import com.ale.common.api.PageResult;
import com.ale.common.constant.ApiConst;
import com.ale.common.entity.user.UserEntity;
import com.ale.common.entity.user.UserListQuery;
import com.ale.common.entity.user.UserQuery;
import com.ale.common.entity.user.UserVO;
import com.ale.common.enums.ResultCode;
import com.ale.user.service.UserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 用户信息接口
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Api(tags = "用户信息接口", description = "UserController")
@RestController
@RequestMapping(ApiConst.API_PREFIX + "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("添加")
    @PostMapping("/save")
    public ApiResult<ResultCode> save(@RequestBody UserQuery userQuery) {
        userService.save(userQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("列表条件分页查询")
    @GetMapping("/pageList")
    public ApiResult<PageResult<UserVO>> pageList(UserListQuery userListQuery) {
        IPage<UserVO> page = userService.pageList(userListQuery);
        return ApiResult.success(PageResult.restPage(page, page.getRecords()));
    }

    @ApiOperation("修改")
    @PostMapping("/update")
    public ApiResult<ResultCode> update(@RequestBody UserQuery userQuery) {
        userService.updateById(userQuery);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("批量删除")
    @PostMapping("/deletes")
    public ApiResult<ResultCode> deletes(@RequestBody List<Long> ids) {
        userService.deletes(ids);
        return ApiResult.success(ResultCode.SUCCESS);
    }

    @ApiOperation("详情")
    @GetMapping("/detail")
    public ApiResult<UserVO> detail(@RequestParam("id") Long id) {
        UserVO vo = userService.detail(id);
        return ApiResult.success(vo);
    }

    @ApiOperation("根据用户名查询")
    @GetMapping("/getUserByName")
    public ApiResult<UserVO> getUserByName(@RequestParam("name") String name) {
        UserVO userVO = userService.getUserByName(name);
        return ApiResult.success(userVO);
    }

}

