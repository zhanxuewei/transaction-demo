package com.ale.user.service;


import com.ale.common.entity.user.UserEntity;
import com.ale.common.entity.user.UserListQuery;
import com.ale.common.entity.user.UserQuery;
import com.ale.common.entity.user.UserVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 添加
     *
     * @param userQuery UserQuery
     * @return 是否成功 true|false
     */
    boolean save(UserQuery userQuery);


    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    boolean deletes(List<Long> ids);

    /**
     * 根据ID修改
     *
     * @param userQuery UserQuery
     * @return 是否成功 true|false
     */
    boolean updateById(UserQuery userQuery);

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return UserDO
     */
    UserVO detail(Long id);

    /**
     * 条件分页查询
     *
     * @param userListQuery
     * @return
     */
    IPage<UserVO> pageList(UserListQuery userListQuery);


    UserVO getUserByName(String name);

}
