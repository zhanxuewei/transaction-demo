package com.ale.user.service.impl;

import com.ale.common.entity.user.UserEntity;
import com.ale.common.entity.user.UserListQuery;
import com.ale.common.entity.user.UserQuery;
import com.ale.common.entity.user.UserVO;
import com.ale.common.mapper.user.UserMapper;
import com.ale.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author zhanxuewei
 * @since 2024-04-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {


    /**
     * 添加
     *
     * @param userQuery UserQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean save(UserQuery userQuery) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userQuery, userEntity);
        userEntity.setPassword(new BCryptPasswordEncoder().encode(userQuery.getPassword()));
        return super.save(userEntity);
    }


    /**
     * 根据ID集合批量删除
     *
     * @param ids id集合
     * @return 是否成功 true|false
     */
    @Override
    public boolean deletes(List<Long> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 根据ID修改
     *
     * @param userQuery UserQuery
     * @return 是否成功 true|false
     */
    @Override
    public boolean updateById(UserQuery userQuery) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userQuery, userEntity);
        return super.updateById(userEntity);
    }

    /**
     * 根据ID查询数据
     *
     * @param id ID
     * @return UserDO
     */
    @Override
    public UserVO detail(Long id) {
        UserEntity userEntity = super.getById(id);
        if (Objects.isNull(userEntity)) {
            return null;
        }
        UserVO vo = new UserVO();
        BeanUtils.copyProperties(userEntity, vo);
        return vo;
    }

    @Override
    public IPage<UserVO> pageList(UserListQuery userListQuery) {
        IPage<UserEntity> pageInfo = new Page<>(userListQuery.getCurrent(), userListQuery.getSize());
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        LambdaQueryWrapper<UserEntity> lambda = queryWrapper.lambda();
        lambda.orderByDesc(UserEntity::getCreateTime);
        IPage<UserEntity> page = super.page(pageInfo, queryWrapper);
        return page.convert(item -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(item, userVO);
            return userVO;
        });

    }

    @Override
    public UserVO getUserByName(String name) {
        QueryWrapper<UserEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(UserEntity::getName, name);
        UserEntity userEntity = super.getOne(queryWrapper);
        if (Objects.isNull(userEntity)) {
            return null;
        }
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userEntity, userVO);
        return userVO;
    }

}
